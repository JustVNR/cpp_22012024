#include "stdafx.h"
#include <algorithm>
#include <vector>
#include <list>
#include <stack>
#include <queue>
#include <set>
#include <map>

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

/*
* STL : STANDARD TEMPLATE LIBRARY
* 
* Ensemble d'outils disponibles sur toutes les plateformes c++ (windows, linux,...)
* 
* - containers : objets qui permet de stocker des collections (vector, list, set, map, stack, queue...)
* - iterators : permettent de parcourir les containes
* - algorithmes : search, sort, cout...
* - ...
*/

using namespace std;

// ATTENTION : n�cessite c+++ 17 ou ult�rieur ( project > properties > c/C++ > Language > c++ Language Standard)
template <template <typename> class CollectionType, typename ItemType>
void print_container(CollectionType<ItemType>& collection)
{
    for (auto item : collection)
        cout << item << " ";
    cout << endl;
}

int main()
{
    SetConsoleOutputCP(1252); 

    COUT("---------------- STL - VECTOR ------------------");

    vector<int> v = { 1, 2,3, 9, 8, 7 };

    // Parcours

    cout << " Parcours vecteur avec boucle for : ";

    for (size_t i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }

    cout << endl;

    cout << " Parcours vecteur avec boucle for (each) : ";

    for (int item : v)
    {
        cout << item << " ";
    }

    cout << endl;

    cout << " Parcours vecteur avec iterateur : ";

    vector<int>::iterator it; // d�claration d'un it�ratur sur vecteur

    for (it = v.begin(); it != v.end(); it++)
    {
        cout << *it << " ";
    }

    cout << endl;

    print_container<vector, int>(v);

    v.pop_back();

    print_container<vector, int>(v);

    v.erase(v.begin() + 1 , v.begin() + 3); // supprime les �l�ments d'indices 1 � 3 (exclu)

    print_container<vector, int>(v);

    COUT("*v.begin() = " << *v.begin()); // 1 
    COUT("*v.rbegin() = " << *v.rbegin()); // 8

    auto ite = v.begin() + 1; // pointe sur le second �l�ment

    COUT("*ite = " << *ite); // 9
    ite = v.insert(ite, 100); // "ite = " pour ne pas invalider l'it�rateur

    print_container<vector, int>(v);

    COUT("*ite = " << *ite); // 100

    it = v.begin(); // On replace l'it�rateur au d�but

    advance(it, 3);

    COUT("*it = " << *it); // 8

    sort(v.begin(), v.end());

    print_container<vector, int>(v);

    v.clear();

    COUT("v.emty() = " << boolalpha  << v.empty()); // boolalpha => pour �crire true � la place de 1 et false � laplace de 0

    TEXT_COLOR(GREEN);
    COUT("\n---------------- STL - LIST ------------------\n");

    list<string> liste = { "bb", "cc" };

    list<string> liste2 = liste; // liste2 est une copie de liste

    liste.push_back("cc");
    liste.push_back("dd");

    print_container<list, string>(liste);

    liste.push_front("aa");

    print_container<list, string>(liste);

    liste.pop_back(); // supprime le dernier �l�ment

    print_container<list, string>(liste);

    //liste.pop_front(); // supprime le premier �l�ment

    //print_container<list, string>(liste);

    // Pour ins�rer un �l�ment dans une liste...

    list<string>::iterator iit(liste.begin()); // Cr�ation d'un it�ratur sur list pointant au d�but de la list "liste"

    advance(iit, 2); // Positionnement de l'it�rateur

    liste.insert(iit, "inserted"); // insertion

    print_container<list, string>(liste);

    liste.remove("cc"); // supprime toutes les occurences de "cc"

    print_container<list, string>(liste);


    COUT("\n--------- PARCOURS DE LIST AVEC ITERATEUR -----------\n");
    // Parcours de list

    //D�claration de 2 it�rateurs pointant respectivement au d�but et � la fin de la list "Liste"
    list<string>::const_iterator lit(liste.begin()), lend(liste.end());

    // Parcours via une boucle for via iterateur

    for (; lit!=lend; lit++)
    {
        cout << *lit << " ";
    }

    // recallage des it�rateurs
    lit = liste.begin();
    lend = liste.end();

    // Parcours via boucle while

    while (lit != lend)
    {
        cout << *lit << " ";
        lit++;
    }

    TEXT_COLOR(YELLOW);
    COUT("\n--------- STL : STACK (LIFO) -----------\n");

    // LIFO : Last In First Out

    stack<int> s;

    for (size_t i = 0; i < 5; i++)
    {
        s.push(i);
    } // 0 1 2 3 4

    COUT("s.size() = " << s.size());
    COUT("s.top() = " << s.top());
    COUT("s.empty() = " << boolalpha << s.empty());

    s.pop(); // supprimer le dernier �l�ment ins�r� (4)
    
    COUT("s.top() = " << s.top()); // 3


    TEXT_COLOR(GREEN);
    COUT("\n--------- STL : QUEUE (FIFO) -----------\n");

    // FIFO : First In First Out

    queue<int> q;

    for (size_t i = 0; i < 5; i++)
    {
        q.push(i);
    } // 0 1 2 3 4

    COUT("q.size() = " << q.size());
    COUT("q.front() = " << q.front());
    COUT("q.back() = " << q.back());
    COUT("q.empty() = " << boolalpha << q.empty());

    q.pop(); // supprimer le dernier premier �l�ment
    q.pop(); // supprimer le dernier premier �l�ment

    COUT("q.front() = " << q.front()); // 2

    TEXT_COLOR(GREEN);
    COUT("\n--------- STL : SET  -----------\n");

    // set : collection d'�l�ments tri�s et sans doublons :

    set<int> mySet;

    mySet.insert(2); // mySet = {2}
    mySet.insert(5); // mySet = {2, 5}
    mySet.insert(2); // mySet = {2, 5} ledoublon n'est pas ins�r�
    mySet.insert(1); //  mySet = {1, 2, 5}
    mySet.insert(10); //  mySet = {1, 2, 5, 10}

    print_container<set, int>(mySet);

    auto ub = mySet.upper_bound(2); // 5 retourne un it�rateur pointant vers le premir �l�m�ent strictement sup�rieur � la valeur pass�e en param�tre

    vector<int> v2 = { 2 , 5, 10, 12, 14, 100 };

    mySet.insert(v2.begin(), v2.end());

    print_container<set, int>(mySet); // 1 2 5 10 12 14 100

    TEXT_COLOR(YELLOW);
    COUT("\n--------- STL : MAP  -----------\n");

    // map : tableau asociatif de type cl�/valeur sans doublons et dont les cl�s sont tri�es

    map<int, string> myMap = { {2, "fifi"} , { 1, "riri"} };

    myMap.insert({ 3, "loulou" });
    myMap.insert({ 3, "not inserted because of already existing key" });

    map<int, string>::const_iterator mit;

    for (mit = myMap.begin(); mit != myMap.end(); mit++)
    {
        COUT("cl� : " << mit->first << ", valeur : " << mit->second);
    }

    COUT("--------------------");

    auto itm = myMap.erase(myMap.find(2)); // erase retourne un it�rateur pointant sur l'�l�ment suivant l'�l�ment supprim�

    COUT("itm->first : " << itm->first << ", itm->second : " << itm->second); // 3 , loulou

    for (mit = myMap.begin(); mit != myMap.end(); mit++)
    {
        COUT("cl� : " << mit->first << ", valeur : " << mit->second);
    }

    // depuis c++17
    for (auto &[key, value] : myMap)
    {
        COUT("cl� : " << key << ", valeur : " << value);
    }
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

