#include "stdafx.h"
#include <vector>
#define _USE_MATH_DEFINES 

#include <math.h>
#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;


/*
* Le polymorphisme permet aux instances d'une sous classe de conserver leurs propri�t�s propres.
* 
* La mise en oeuvre du polymorphisme repose sur les concepts :
* - d'h�ritage
* - de resolution dynamique des liens (par oppositon � la r�sloution statique des liens)
* 
* - r�solution statique des liens : 
*   - le choix de la m�thode � appelr est effectu� � l compilation
*   - la m�thode appel�e est n�cessairement celle du type de la variable pass�e en param�tre (classe m�re)
* 
* - r�solution dynamique des lines : 
*   - le choix de la m�thode � ex�cuter se fait � l'ex�cution
*   - en tenant compte de la r�elle nature de l'instance consid�r�e (classe fille)
* 
* La r�solution dynamique des liens exige :
*   - 1 : que la m�thode concern�e soit d�clar�e comme "virtual" dans la classe m�re
*   - 2 : qu'elle s'applique � des objets pass�s par r�f�rence ou par pointeur
*/

class Personnage {
public:
    // sans virtual : r�solution statique des liens 
    // avec virtual : r�solution dynamique des liens ssi m�thode appel�e sur r�f�rence ou pointeur
    virtual void afficher() const { COUT("Personnage affich�"); }
    virtual void rencontrer(Personnage& p) const { COUT("Personnage dit bonjour"); }

    // les m�thodes "afficher" et "rencontrer" des classes filles seront implicitement consid�r�es comme virtual par transitivit�
};

class Voleur : public Personnage
{
public:
   
    /*virtual implicite*/ void afficher() const override { COUT("Voleur affich�"); } // override permet d'ptre certain que la m�thode est bien marqu�e 'virtual' dans la classe m�re
    /*virtual implicite*/ void rencontrer(Personnage& p) const override { COUT("Voleur dit bonjour"); }
};


class Guerrier : public Personnage
{
public:

    /*virtual implicite*/ void afficher() const override { COUT("Guerrier affich�"); }
    /*virtual implicite*/ void rencontrer(Personnage& p) const override { COUT("Guerrier dit bonjour"); }
};

void faire_rencontrer_stat(Personnage p1, Personnage p2)
{
    p1.rencontrer(p2); // p1 n'est ni une r�f�rence ni un pointeur => r�solution statique des liens
}

void faire_rencontrer(Personnage& p1, Personnage& p2)
{

    /*
    * - Si :
    *   - m�thode "rencontrer" virtuelle dans classe m�re ET
    *   - p1 est une r�f�rence ou un pointeur 
    *   => r�solution dynamique des liens
    * 
    * - Sinon : r�solution statique des liens
    */
    p1.rencontrer(p2); // p1 est une r�f�rence ET 'rencontrer' est 'virtual' dans 'Personnage' => r�solution dynamique des liens (comportment poymorphique)
}

// ---------------------------------------------
// ---------------------------------------------

class Mammifere
{
public:
    Mammifere()
    {
        COUT("Un mammif�re est n�");
    } // un constructeur ne peut pas �tre 'virtual'

    virtual ~Mammifere()
    {
        COUT("Un mammif�re est mort");
    } // Bonne pratique : destructeur 'virtual' (pour pouvoir lib�er les ressources des classes filles)

    void manger() { COUT("Un mammif�re mange."); }

    virtual void bouger() { COUT("Un mammif�re bouge"); }
};

class Dauphin : public Mammifere
{
public:
    Dauphin() { COUT("Un dauphin est n�"); } 

    ~Dauphin() { COUT("Un dauphin est mort"); } 

    void manger() { COUT("Un dauphin mange."); }

    void bouger() { COUT("Un dauphin bouge"); }
};


// ----------------------------------------------------------------------------
// ---------------- MASQUAGE, SUBSTITUTION, SURCHARGE -------------------------
// ----------------------------------------------------------------------------

// - Surcharge (overlading) : m�me nom de m�thode mais signatures diff�rentes DANS LA MEME PORTEE
// - Masquage (shadowing) : entit�s de m�me nom mais de port�es diff�rentes, masqu�es par les r�gles de r�siolution  de port�e : la port�e la plus proche masque les port�es plus lointaines => une seule m�thode de m�me nom suffit � les masquer toutes, ind�pendamment de leur signature
// - Substitution (overriding) : red�finition de m�thodes virtuelles :si une classe fille ovveride une seule m�thode parmi plusieurs suurcharg�es dans la classe m�re, toutes les m�thodes portant le m�me nom de la classe m�re sont masqu�es m�mes si elles ne sont pas overrid�es.

class A
{
public:
    virtual void method(int i) const { COUT("A::method(int) : " << i); }

    // surcharge
    virtual void method(string const& s) const { COUT("A::method(string) : " << s); }

    // le mot cl� "final" interdit la red�finition
    virtual void methodFinal() const final { COUT(" "); }

};

class B : public A
{
public:
    /*virtual implicite*/ void method(string const& s) const override { COUT("B::method(string) : " << s); }

    // la classe 'B' n'override que "void method(string const& s) const"
    // mais la m�thode "void method(int i) const" de la classe A est quand m�me masqu�e

    // virtual void methodFinal() const { COUT(" "); } KO cannot ovveride final method
};

class C : public A
{
public:
    // nouvelles signature : les m�thodes nomm�e 'method' des classe m�res sont toutes masqu�es quelque soient leurs signatures
    virtual void method(double d) const { COUT("C::method(double) : " << d); }
};


// ------------------------ CLASSES ABSTRAITES -------------------------

/*
* Une classe abstraite est une classe contenant au moins une m�thode virtuelle pure (abstraite)
* Une classe abstraite ne peut pas �tre instanci�e (mais peut avoir des constructeurs)
* Ses sous classes restent elle-m�mes abstraites tant qu'elles n'impl�mentent pas toutes les m�thodes virtiuelles pures de la classe m�re
*/

class Shape
{
public:
    Shape()
    {
        COUT("constructeur de SHAPE\n");
    }

    virtual double perimetre() const = 0; // Methode virtuelle pure

    virtual double surface() const  = 0;
};



class Cercle : public Shape
{
private :
    double _r;
public:
    Cercle(double r)
    {
        _r = r;
    }

    double perimetre() const override
    {
        return M_PI * _r * 2;
    }

    double surface() const override
    {
        return M_PI * _r * _r;
    }
};

class Carre : public Shape
{
private:
    double _c;
public:
    Carre(double c)
    {
        _c = c;
    }

    double perimetre() const override
    {
        return _c * 4;
    }

    double surface() const override
    {
        return _c * _c;
    }
};


// --------------------------- COLLECTIONS HETEROGENES -------------------------------------

class Jeu
{
private:
    // vector<Personnage> personnages; // la collection peut comprendre des personnages de types diff�rents => collection h�t�rog�ne MAIS la collection ne permet pas de comportement polymorphique de ses �l�m�ents car il ne s'agit d'une collection de pointeurs ni d'une collection de r�f�rences.

    vector<Personnage*> personnages; // Collection de pointeurs => possibilit� de comportement polymorphique

    //vector<unique_ptr<Personnage>> personnages; // pointeurs intelligents c++11

    // Attention : il faut que les personnages point�s par chaque �l�ment de la collection existent et continuenet d'exister tant que la collection existe.

public:
    void afficher() const;
    void ajouterPersonnage(Personnage* nouveau);
    void clearPersonnages();
};

void Jeu::ajouterPersonnage(Personnage* nouveau)
{
    if (nouveau != nullptr) {
        personnages.push_back(nouveau);
        //personnages.push_back((unique_ptr<Personnage>)nouveau); // cast de pointeur � la C vers smart pointer
    }
}

void Jeu::afficher()const
{

    //for (auto quidam : personnages) { // si utilisation de unique_ptr, il faut obligatoirement passer quidam par r�f�rence
    //    quidam->afficher();
    //}

    for (auto const& quidam : personnages) { // si utilisation de unique_ptr, il faut obligatoirement passer quidam par r�f�rence
        quidam->afficher(); // quidam appel� par r�f�rence et 'afficher' est virtuelle dans 'Personnage' => comportement polymorphique
    }
}

void Jeu::clearPersonnages()
{
    for (auto quidam : personnages)
    {
        delete quidam;
    }

    personnages.clear(); // vide la collection
}

int main()
{
    SetConsoleOutputCP(1252); 

    Guerrier g;

    Voleur v;

    faire_rencontrer_stat(g, v); // Personnage dit bonjour

    faire_rencontrer(g, v); // Guerrier dit bonjour

    TEXT_COLOR(GREEN);
    COUT("\n-----------------------------------------------\n");
    
    Mammifere* dauphin = new Dauphin(); // Le constructeur de Dauphin commence par appeler celui de Mammifere

    // (*dauphin).bouger(); // <=> dauphin->bouger();
    dauphin->bouger(); // "bouger" est virtuelle ET appel�e via pointeur => r�solution dynamique des liens => Un Dauphin bouge

    delete dauphin; // destructeur virtuel : re�solution dynamique des liens : ~Dauphin (qui appelle ~Mammifere)

    TEXT_COLOR(YELLOW);
    COUT("\n-------------------MASQUAGE SUBSTITUTION SURCHARGE ----------------------------\n");

    B b;

    b.method("2");

    //b.method(2); // NOK : la m�thode est masqu�e

    b.A::method(2); // OK : Pour y acc�der il faut la d�masquer gr�ce � l'op�rateur de r�solution de port�e en appelant explicitement la m�thode de la classe 'A'

    C c;

    c.method(2); // OK : Appel de la m�thode C::method(double)

    //c.method("2"); // NOK : A::method(string) est masqu�e
    c.A::method("2"); // OK : A::method(string)
    c.A::method(2); // OK : A::method(int)

    A* pa = nullptr;

    pa = &b;

    pa->method("2"); // B::method(string) car method(string) virtuelle dans A et appel�e sur pointeur
    pa->method(2);   // A::method(int) car "method(int)" non red�finie dans B

    pa = &c;

    pa->method("2"); // A::method(string) car "method(string)" non red�finie dans C
    pa->method(2.1); // A::method(int) : 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! le double est converti en 'int' !!!

    //pa->C::method(2.1); // NOK : "A" n'h�rite pas de "C"

    TEXT_COLOR(GREEN);
    COUT("------------------------ CLASSES ABSTRAITES -------------------------");

    //Shape shape; // NOK : impossible d'instancier une classe abstraire

    Cercle cercle(2.0);
    Carre carre(2.0);

    COUT(cercle.perimetre());

    TEXT_COLOR(YELLOW);
    COUT("------------------------ COLLECTIONS HETEROGENES -------------------------");

    Jeu jeu;

    jeu.ajouterPersonnage(new Guerrier()); // Avec pointeurs � la C il faut d�salouer la m�moire via appel � "clearPersonnages()"
    jeu.ajouterPersonnage(new Guerrier());
    jeu.ajouterPersonnage(new Voleur());

    jeu.afficher();

    jeu.clearPersonnages();
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

