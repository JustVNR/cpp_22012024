#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;

class Ovipare
{
protected:
    unsigned short _nOeufs;

public:
    Ovipare(unsigned short nOeufs) :_nOeufs(nOeufs) {}

    void afficher() const
    {
        COUT("Nombres d'oeufs : " << _nOeufs);
    }
};


class Vivipare
{
protected:
    unsigned short _gestation;

public:
    Vivipare(unsigned short gestation) :_gestation(gestation) {}

    void afficher() const
    {
        COUT("Dur�e de gestation : " << _gestation);
    }
};

/*
* Comme pour l'h�ritage simple, l'initialisation des attributs h�rit�s doit �tre faite par invocation des constructeurs des super-classes.
* 
* MAIS l'appel des constructeurs des super-classes se fait :
* - SELON L'ORDRE DE DECLARATION D'HERITAGE
* - ET NON PAS selon l'ordre d'appel des constructeurs dans le constructeur de la classe fille.
* 
* Il n'est pas n�cessaire d''appeler explicitement les constructeurs sans param�tres des classes m�res si elles en poss�dent.
* 
* Les destructeurs sont appel�s dans l'ordre inverse de celui des constructeurs
*/
class Ovovivipare : public Ovipare, public Vivipare
{
protected:
    bool _especeProtegee;

public:
    // ATTENTION : le constructeur de Ovipare sera appel� avant celui de Vivipare car Ovovivipare h�rite de Ovipare avant d'hariter de Vivipare
    Ovovivipare(unsigned short nOeufs, unsigned short gestation, bool protege) : Vivipare(gestation), Ovipare(nOeufs), _especeProtegee(protege){}

    // Solution 2 : choisir une des 2 m�thodes parmi les classes m�res
    // using Vivipare::afficher; // utilsation de la m�thode afficher de la classe Vivipare

    // Solution 3 : impl�menter la m�thode
    void afficher() const
    {
        Ovipare::afficher();
        Vivipare::afficher();
    }
};

int main()
{
    SetConsoleOutputCP(1252); 

    Ovovivipare o(3, 4, false);

    /*
    * ATTENTION : L'acc�s � la m�thode afficher provoquera une erreur de compilation, et ce m�me si les m�thodes afficher avaient des signatures diff�rentes dans chacune des 2 classes m�res.
    */
    // o.afficher();

    // POur r�soudre le probl�me 

    // SOLUION 1 : utiliser l'op�rateur de r�solution de port�e pour choisir quelle m�thode utiliser

    o.Vivipare::afficher(); // Mauvaise pratique car la mani�re d'afficher un Ovovivipare incombe en cons�quence � l'utilisateur de la classe Ovovivipare alors que cette responsabilit� devrait incomber au(x) concepteur(s) de la classe

    /*
    * Pour appeler directement la m�thode 'afficher' il faut : 
    * - soit la red�finir dans la classe Ovovivipare
    * - soit sp�cifier explicitement (avec le mot cl� 'using') laquelle des m�thodes des super-classes utiliser
    */
    o.afficher();

}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

