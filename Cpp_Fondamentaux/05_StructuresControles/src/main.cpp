#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

int main()
{
	SetConsoleOutputCP(1252);


	COUT("*****************************");
	COUT("******** CONDITIONS ***************");
	COUT("*****************************\n");

	int n = 15;

	if (n > 10)
		// { on peut ommettre les accolades d'un bloc d'instructions lorsqu'il ne contient qu'une instruction
		COUT("n est sup�rieur � 10");
	// }
	else if (n < 10)
	{
		COUT("n est inf�rieur � 10");
	}
	else
	{
		COUT("n est �gal � 10");
	}

	// Op�rateur ternaire : ani�re condens�e d'�crire un if else

	int time = 20;

	// sans op�rateur ternaire
	if (time < 18)
	{
		COUT("Bonjour");
	}
	else
	{
		COUT("Bonsoir");
	}

	// avec op�rateur ternaire
	// 
	// syntaxe : CONDITION ? SI VRAI : SI FAUX

	COUT((time < 18 ? "Bonjour" : "Bonsoir"));

	// SWITCH

	int note = 10;
	if (note < 10)
	{
		COUT("�chec");
	}
	else if (note == 10)
	{
		COUT("re�u");
	}
	else if (note < 12)
	{
		COUT("re�u"); // pour ex�cuter cette ligne il faudra d'abord v�rifier SEQUENTIELLEMENT que toutes les conditions pr�c�dentes soient fausses (peu performant)
	}
	// etc... 

	switch (note)
	{
	case 10:
		COUT("re�u");
		break;
	case 12:
		COUT("assez Bien");
		break;
	case 14:
		COUT("mention Bien"); // il n'est pas n�cessaire de v�rifier tous mes case pr�c�dents pour ex�cuter celui ci...
		break;
	default:
		COUT("note ne vaut ni 10 ni 12 ni 14");
	}
	TEXT_COLOR(GREEN);
	COUT("*****************************");
	COUT("******** BOUCLES ***************");
	COUT("*****************************\n");

	COUT("BOUCLE FOR");
	for (int i = 0; i < 5; i++) // (d�claration; condition d'arr�t; incr�mentation)
	{
		COUT(i);
	}
	COUT("-----");
	// ATTENTION AUX BOUCLES INFINIES : i > -5 sera toujour vrai !! BOUCLE INFINIE
	//for (int i = 0; i > -5; i++) // 
	//{
	//	COUT(i);
	//}

	for (int i = 0; i > -5; i--)
	{
		COUT(i);
	}
	COUT("----- BREAK\n");

	for (int i = 0; i < 5; i++)
	{
		if (i == 2)
		{
			break; // SORT DE LA BOUCLE
		}

		COUT(i);
	}

	COUT("----- CONTINUE\n");

	for (int i = 0; i < 5; i++)
	{
		if (i == 2)
		{
			continue; // PASSE DIRECTEMENT A L'ITERATION SUIVANTE
		}

		COUT(i);
	}

	COUT("-----");

	COUT("BOUCLE FOR (TYPE FOR EACH)");

	int tableau[5] = { 0, 1, 2, 4 , 3 }; // d�claration d'un tableau de 5 entiers

	for (int item : tableau)
	{
		COUT(item);
	}

	for (auto item : tableau) // auto : inf�rence de type : le compilateur peut d�terminer le type de l'item � partir du type du tableau
	{
		COUT(item);
	}


	COUT("\n************** EXO MOYENNE ***************\n");

	// Demander � l'utilisateur de saisir un nombre de notes (enti�res).
	// Demander � l'utilisateur de saisir chacune des notes. 
	// Calculer et afficher la moyenne des notes saisies

	int nNotes = 0;
	double somme = 0.0;

	std::cout << "Entrez le nombre de notes : ";
	std::cin >> nNotes;

	if (nNotes > 0) {

		for (int i = 1; i <= nNotes; i++)
		{
			int currentNote;

			std::cout << "Saisir la note N� " << i << " : ";
			std::cin >> currentNote;

			somme += currentNote;
		}
	}

	COUT("La moyenne vaut " << somme / nNotes);


	TEXT_COLOR(YELLOW);
	COUT("\n************* BOUCLES CONDITIONNELLES ************\n");

	/*
	* Boucle while : le bloc d'instructions est r�p�t� tant que la condition est �valu�e � true.
	*/

	COUT("\n************* WHILE ************\n");

	int i = 0;

	while (i < 5)
	{
		COUT(i);
		i++; // ATTENTION A NE PAS OUBLIER D'INCREMENTER LA VALEUR DE i SOUS PEINE DE BOUCLE INFINIE
	}

	COUT("----------");

	while (true)
	{
		COUT(i);

		i++;

		if (i == 10) break;
	}

	COUT("\n************* DO WHILE ************\n");
	/*
	* Boucle do while : identique � la boucle while sauf que la condition est �valu�e APRES l'ex�cution du bloc d'instructions.
	* 
	* On est donc certain de paser au moins une fois dans la boucle et ce m�me si la condition �tait intialement fausse
	* 
	*/

	do
	{
		COUT("Veuillez saisir le nombre de notes");
		std::cin >> nNotes;

	} while (nNotes < 0);


	COUT("\n************** EXO CALCULATRICE ***************\n");
	// Exercice: Calculatrice
	// Saisir dans la console :
	// - un nombre � virgule flottante a
	// - un caract�re 'op�rateur' qui aura pour valeur valide "+", "-", "*" ou "/" 
	// - un nombre � virgule flottante b
	// Afficher:
	// - Le r�sultat de l�op�ration
	// - Un message d�erreur si l�op�rateur est incorrect
	// - Un message d�erreur si l�on fait une division par 0

	double a, b;
	char op;

	std::cout << "Saisir un double : ";
	std::cin >> a;

	std::cout << "Saisir un op�rateur ('+', '-', '*', '/') : ";
	std::cin >> op;

	std::cout << "Saisir un double : ";
	std::cin >> b;

	switch (op)
	{
	case '+':
		COUT(a << " + " << b << " = " << (a + b));
		break;
	case '-':
		COUT(a << " - " << b << " = " << (a - b));
		break;
	case '*':
		COUT(a << " * " << b << " = " << (a * b));
		break;

	case '/':
		if (b == 0.0)
		{
			COUT("Erreur : division par 0");
		}
		else
		{
			COUT(a << " / " << b << " = " << (a / b));
		}
		break;
	default:
		COUT("op�rateur '" << op << "' invalide");
	}
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

