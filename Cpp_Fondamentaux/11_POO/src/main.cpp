#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

namespace figures
{
    class Rectangle {
    private:
        double _largeur;
        double _longueur = 0.0; // On peut initialiser l'attribut ici. 

    public:

        // Getters et Setters
        double largeur() const
        {
            return _largeur;
        }

        void largeur(double value)
        {
            _largeur = value;
        }

        double longueur() const
        {
            return _longueur;
        }

        void longueur(double value)
        {
            _longueur = value;
        }

        // Constructeurs

        /*
        Rectangle()
        {
            _largeur = 0.0;
            _longueur = 0.0;
        }

        Rectangle(double largeur, double longueur)
        {
            _largeur = largeur; // On peut aussi appeler les setters ici (c'est mieux)
            _longueur = longueur;
        }*/

        // Constructeur avec liste d'initialisation
        Rectangle(double largeur, double longueur) : _largeur(largeur), _longueur(longueur) { }

        // Constructeur sans param�tre faisant appel au constructeur � 2 param�tres pour fixer des valeur par d�faut
        Rectangle() : Rectangle(1.0, 2.0){}

        // Constructeur de copie
        // Un constructeur de copie par d�faut est automatiquement g�n�r� par le compilateur s'il n'est pas explicitement impl�ment�
        // Le contructeur par d�faut op�re une initialisation membre � membre des attributs
        // Il cependant parfois n�cessaire de red�finir le constructeur de copie.
        // Notamment lorsque certains des attributs sont des pointeurs
        // Auquel cas il faudra r�aliser une copie profonde pour que la copie et l'originale ne pointe pas sur les m�mes adresses
        Rectangle(Rectangle const& other) : _largeur(other._largeur), _longueur(other._longueur){}

        // On peut aussi nterdire la copie
        // Rectangle(Rectangle const& other) = delete;


        // Destructeur
        // Ne peut pas �tre appel� directement
        // pas de param�tres
        // pas de surcharges
        // appel� automatiquement par le garbage collector
        ~Rectangle()
        {
            COUT("Rectangle destroyed");
        }

        // M�thodes
        double Surface() const
        {
            return _largeur * _longueur;
        }
    };

    class RectangleDyn
    {
    private:
        double* _largeur = new double(2.0);
        double* _longueur = new double(4.0);

        /*
        * Attribut de classe d�fini avec le mot cl� 'static'
        * - peut �tre priv� ou public ou protected...
        * - partag� par toute les instances de la classe
        * - existe ind�endamment de toute instance
        * - doit �tre initialis� explicitment � l'ext�rieur de la classe
        */
        static int _count;
    public:

        RectangleDyn()
        {
            _count++; // Attention : ne sera pas incr�ment� par le constructeur de copie par d�faut...
        }
        ~RectangleDyn() {
            COUT("RectangleDyn destroyed !");
            delete _largeur; // Va vcauser une xception si on utilise le constructeur de copie par d�faut
            delete _longueur; // Par ce que le d�structeur sera appel� plusieurs fois pour lib�rer les m�mes ressources...
            _count--;
        }

        RectangleDyn(RectangleDyn const& other)
        {
            _largeur = new double(*other._largeur); // deep copy : on ne se contente pas de copiers la valeur du pointeur
            _longueur = new double(*other._longueur); // On cr�e un nouveau pointeur et on l'initialise avec la valeur point�e par le pointeur copi�
            _count++;
        }

        // M�thodes
        double Surface() const
        {
            return (*_largeur) * (*_longueur);
        }

        /*
        * M�thode de classe : 
        * - peut petre utilis�e sans objet (instance)
        * - ne peut acc�der qu'� des attributs et m�thodes statiques (et donc pas d'intance)
        */
        static int GetCount()
        {
            return _count;
        }
    };

    int RectangleDyn::_count = 0; // On ne peut pas initialiser un attribut static direcment dans la classe...
}
int main()
{
    SetConsoleOutputCP(1252); 

    figures::Rectangle rect(2.0, 4.0);

    rect.longueur(6.0); // Appel au Setter

    COUT("rect.longeur() = " << rect.longueur());
    COUT("rect.Surface() = " << rect.Surface());

    figures::Rectangle defaultRect; // Appel du constructeur sans param�tre
    COUT("defaultRect.Surface() = " << defaultRect.Surface());

    figures::Rectangle rectCopied(rect); // Appel au cconstructeur de copie

    COUT("rectCopied.Surface() = " << rectCopied.Surface());

    TEXT_COLOR(GREEN);
    COUT("\n------------- ALLOCATION DYNAMIQUE -----------------\n");

    COUT("figures::RectangleDyn::GetCount() = " << figures::RectangleDyn::GetCount()); // 0

    {
        figures::RectangleDyn rectDyn;

        COUT("figures::RectangleDyn::GetCount() = " << figures::RectangleDyn::GetCount()); // 1

        COUT("rectDyn.Surface() = " << rectDyn.Surface());
    }

    COUT("figures::RectangleDyn::GetCount() = " << figures::RectangleDyn::GetCount()); // 0

    COUT("------------ COPIE ----------------");

    {
        COUT("figures::RectangleDyn::GetCount() = " << figures::RectangleDyn::GetCount()); // 0

        figures::RectangleDyn rectDyn;

        COUT("figures::RectangleDyn::GetCount() = " << figures::RectangleDyn::GetCount()); // 1

        figures::RectangleDyn rectDyn2(rectDyn); // copie de rectDyn

        COUT("figures::RectangleDyn::GetCount() = " << figures::RectangleDyn::GetCount()); // 2
    }

    COUT("figures::RectangleDyn::GetCount() = " << figures::RectangleDyn::GetCount()); // 0
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

