#include "stdafx.h"
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;


/*
* "struct" permet de cr�er de nouveaux types permettant :
* - regrouper un ensemble de donn�es de types potentiellment diff�rentes
* - faire retourner plusieurs valeurs � une fonction
* - simplifier la conception des programmes
*/

struct Date
{
	int jour;
	int mois;
	int annee;
};

struct Personne
{
private:
	int age;

	/*
	* Contrairement au type "class" tous les champs d'une structure sont "public" par d�faut
	* et sont donc accessibles acesibles n'importe ou et par n'importe qui
	* ce qui viole l'une des concepts fondamentaux de la P.O.O. : le principe d'encapsulation
	*/
public:
	string nom;
	double taille;
	char sexe;



	// Constructeur : m�thode portant le m�me nom que la structure et qui ne sp�cifie pas de type de retour
	// Un constructeur permet d'instancier des objets du type consid�r� (ici de type Personne)


	// Constructeur sans param�tre : implicitement cr�� par d�faut en l'absence de tout autre constructeur
	// Mais doit �tre au besoin d�clar� explicitement si d'autre(s) constructeur(s) sont d�finis
	Personne()
	{

	}

	// Un constructeur peut �tre surcharg�
	// Constructeur � 3 param�tres
	Personne(string n, double t, int a)
	{
		nom = n;
		taille = t;
		age = a;
	}


	// Constructeur � 4 param�tres
	Personne(string n, double t, int a, char s)
	{
		nom = n;
		taille = t;
		age = a;
		sexe = s;
	}

	// Destructeur
	// - n'accepte pas de param�tre
	// - ne peut pas �tre surcharg�
	// - ne peut pas �tre appel� explicitement
	// - automatiquement appel� lors de la destruction de l'objet
	~Personne()
	{
		COUT(nom << " est d�truit");
	}

	string toString() const
	{
		ostringstream oss; // #include <sstream> Output stream string
		oss << setprecision(3) << taille; // #include <iomanip>

		return "nom = " + nom + " - taille = " + oss.str() + " - age = " + to_string(age) + " - sexe = " + sexe;
	}

	int GetAge() const // la m�thode ne peut pas modifier l'objet
	{
		// age++; // KO : le mot cl� 'const' interdit la modification de l'instance en cours

		nbAccessGetAge++; // OK car 'nbAccessGetAge' est d�finie comme mutable

		return age;
	}

	void SetAge(int a)
	{
		// ICI on peut mettre une logique permettant de v�rifier la validit� du param�tre (par exemple : age soit positif)
		age = a;
	}

	void Anniversaire()
	{
		age++;
		nbAnniversaires++;
	}

	static int getNbAnniversaires()
	{
		return nbAnniversaires;
	}

private:
	mutable int nbAccessGetAge = 0;

	// Une variable statique est partag�e par toutes les instances de la structure / classe
	static int nbAnniversaires; // les variables statiques doivent �tre d�clar�es en dehors de la structure / classe
};

int Personne::nbAnniversaires = 0; // Initialisation de la variable statique

// typedef permet de cr�er des alias pour les types existants.
typedef vector<Personne> Personnes;


// Cr�er une fonction CreatePersnne qui : 
// - demande � l'utilisateur de renseigner l'ensemble des paraam�tres associ�s � un objet de type "Personne"
// - les sexe renseign� devra �tre soit 'M', 'm', 'F' ou 'f'
// - retourne l'objet ainsi cr��

Personne CreatePersonne()
{
	Personne p;

	COUT("Saisie d'une nouvelle personne :");
	cout << "Nom : ";
	//cin >> p.nom; // ne r�cup�re qu'un mot
	getline(cin, p.nom); // r�cup�re toute la ligne

	cout << "Taille : ";
	cin >> p.taille;

	cout << "Age : ";
	int age;
	cin >> age;

	p.SetAge(age);

	do
	{
		string sexe;

		cout << " Femme [F] / Homme [M]";

		cin >> sexe;

		p.sexe = toupper(sexe[0]);

	} while (p.sexe != 'M' && p.sexe != 'F');

	return p;
}

void AfficherPersonne(Personne const& p) // passage par r�f�rence constante pour ne pas copier l'objet et pour interdire sa modification
{
	COUT(p.toString());
}
int main()
{
	SetConsoleOutputCP(1252);

	Date aujourdhui = { 23, 01, 2024 };

	COUT("Jour = " << aujourdhui.jour << ", mois = " << aujourdhui.mois << ", ann�e = " << aujourdhui.annee);

	Personne riri;

	riri.nom = "riri";
	riri.sexe = 'M';
	riri.taille = 1.2;
	//riri.age = 12; KO : age est d�sormais priv�
	riri.SetAge(12);

	Personne fifi("fifi", 1.2, 12, 'M');

	COUT(fifi.toString());

	{
		Personne loulou("loulou", 1.2, 12, 'M');
	}

	COUT("Personne::getNbAnniversaires() = " << Personne::getNbAnniversaires()); // Une variable / m�thode statique / de classe est appel�e � partir du nom de la struture /classe

	riri.Anniversaire();

	COUT(riri.toString());

	COUT("Personne::getNbAnniversaires() = " << Personne::getNbAnniversaires());

	TEXT_COLOR(GREEN);
	COUT("\n-------------- TABLEAU DE PERSONNES --------------------\n");

	//vector<Personne> personnes =
	Personnes personnes = // Utilisation d'un alias (typedef)
	{
		Personne("Daisy", 0.75, 30, 'F'),
		Personne("Donald", 0.75, 40, 'M'),
		Personne("Picsou", 0.75, 70, 'M')
	};

	for (auto& p : personnes) {
		COUT(p.toString());
	}

	Personne createdPersonne = CreatePersonne();

	// COUT(createdPersonne.toString());

	AfficherPersonne(createdPersonne);

}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

