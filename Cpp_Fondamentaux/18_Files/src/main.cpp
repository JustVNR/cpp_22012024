#include "stdafx.h"
#include <fstream> // fstream requiert iostream (d�j� pr�sent dans stadf.h)
#include <string>
#include <sstream>

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;

int main()
{
    SetConsoleOutputCP(1252); 

    fstream myFile;
    myFile.open("cppfile.txt", ios::out); // ios::out : ouverture en �criture (�crase le contenu existant)

    if (myFile.is_open())
    {
        myFile << "Hello World to text file\n";
        myFile << "Hello World to text file\n";
        myFile.close();
    }

    myFile.open("cppfile.txt", ios::app); // ios::app : ouverture en mode append (�crit � la suite)

    if (myFile.is_open())
    {
        myFile << "Hello World appened to text file\n";
        myFile.close();
    }

    myFile.open("cppfile.txt", ios::in); // ios::in : ouverture en lecture

    if (myFile.is_open())
    {
        string line;

        while (getline(myFile, line)) // getline dans <string> ( � inclure)
        {
            COUT(line);
        }

        myFile.close();
    }

    COUT("\n--------------- AJOUT AU DEBUT DU FICHIER ----------------\n");

    myFile.open("cppfile.txt", std::ios::in | std::ios::out); // ouverture en mode lecture et �criture

    if (myFile.is_open())
    {
        stringstream existingContent; // #include <sstream>
        existingContent << myFile.rdbuf();

        myFile.seekp(0, std::ios::beg);

        myFile << "Hello World appened to begin of text file\n";
        myFile << existingContent.str();
        myFile.close();
    }

    myFile.open("cppfile.txt", ios::in); // ouverture en lecture

    if (myFile.is_open())
    {
        string line;

        while (getline(myFile, line)) // getline dans <string> (� inclure)
        {
            COUT(line);
        }
        myFile.close();
    }
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

