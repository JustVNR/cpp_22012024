#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl

#define TEXT_COLOR(X) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),X)

#define GREEN	10
#define CYAN	11
#define RED		12
#define MAGENTA	13
#define YELLOW	14
#define WHITE	15

int main()
{
	SetConsoleOutputCP(1252);

	COUT("************************************");
	COUT("************** ARITHMETIQUES *****************");
	COUT("************************************\n");
	
	int a = 3, b = 6;

	COUT(a + b);
	COUT(a - b);
	COUT(a * b);
	COUT(a / b); // 0 : division enti�re
	COUT(a / (b * 1.0)); // 0.5

	// Modulo : retourne le reste de la division enti�re
	// 7 / 2 = 3 reste 1 => 7 % 3 = 1
	COUT(a % b);  // 3 / 6 = 0 reste 3 => 3 % 6 = 3 

	int n;

	double x = 1.5;

	n = 3 * x;

	COUT(n); // 4 : arrondi de 4.5 car 'n' est de type 'int'

	COUT("\n************************************");
	COUT("************** AFFECTATION COMPOSEE *****************");
	COUT("************************************\n");

	
	a += b; // <=<> a = a + b;
	COUT(a);
	a -= b; // <=<> a = a - b;
	COUT(a);
	a *= b; // <=<> a = a * b;
	COUT(a);
	a /= b; // <=<> a = a / b;
	COUT(a);

	COUT("\n************************************");
	COUT("************** INCREMENTATION *****************");
	COUT("************************************\n");
	
	COUT("a = " << a); // 3

	// a = a + 1; <=> a+=1 <=> a++

	COUT(a++); // 3 : post incr�mentation : On affiche d'abord a puis on l'incr�mente
	// A partir d'ici a = 4...
	COUT(++a); // 5 : pr� incr�mentation : On incr�mente d'abord puis on affiche. 4 + 1 = 5

	COUT(a--); // 5
	COUT(a); // 4
	COUT(--a); // 3

	COUT("\n************************************");
	COUT("************** COMPARAISON *****************");
	COUT("************************************\n");

	COUT((a == b)); // retourne true si a est �gal � b
	COUT((a > b)); // retourne true si a est strictement sup�rieur � b
	COUT((a >= b)); // retourne true si a est sup�rieur ou �gal � b
	COUT((a < b)); // retourne true si a est strictement inf�rieur � b
	COUT((a <= b)); // retourne true si a est inf�rieur ou �gal � b
	COUT((a != b)); // retourne true si a est diff�rent de b


	COUT("\n************************************");
	COUT("************** LOGIQUES *****************");
	COUT("************************************\n");

	COUT((a > b && a < b)); // ET : && 
	COUT((a > b || a < b)); // OU :||
	COUT((a > b ^ a < b)); // OU EXLUSIF : ^ : retourne true si et seuelement si une seule des condisiotns est vraie

	bool myBool = a < b;

	myBool = !myBool;

	COUT("myBool = " << myBool);

	COUT(!(a < b)); // NON : ! (transforme true en false et r�ciproquement)


	COUT("\n************************************");
	COUT("************** (BITWISE OPERATORS) *****************");
	COUT("************************************\n");

	a = 5; // binaire 0101
	b = 3; // binaire 0011

	int result = a & b; // 0001 => 1

	result = a | b; // 0111 => 7

	result = a ^ b; // 0110 => 6

	int resultLeft = a << 1; // 0101 << 1 = 1010 => 8 + 2 = 10
	COUT("resultLeft = " << resultLeft);

	int resultRight = a >> 1; //  0101 >> 1 = 0010 => 2
	COUT("resultRight = " << resultRight);

}
