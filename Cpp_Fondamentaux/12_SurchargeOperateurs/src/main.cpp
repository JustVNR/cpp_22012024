#include "stdafx.h"
#include <string>
#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;

/*
* Il existe 2 types de surcharges d'op�raateurs : 
* - interne
* - externe
* 
* La surcharge interne est impossible pour eds op�rateur dont l'op�rande de gauche n'est pas du type consid�r�
* 
* exemple : p * 3 : p (qui est � gauche) est de type Point => surcharge interne possible
*           3 * p : 3 (qui est � gauche) n'est PAS de type Point => surcharge interne IMPOSSIBLE
*/

class Point
{
    double _x;
    double _y;

    friend const Point operator*(double mult, Point const& p); // la fonction est d�clar�e en tant qu'amie et aura donc le droit d'acc�der aux champs priv�s de la classe (mauvaise pratique car viole le principe d'encapsulation)

public:

    // Guetters
    double x() const
    {
        return _x;
    }

    double y() const
    {
        return _y;
    }

    // Constructor

    Point(double x, double y): _x(x), _y(y){}

    // M�thodes

    Point add(Point const& other)
    {
        return Point(_x + other._x, _y + other._y);
    }

    Point multiply(double mult)
    {
        return Point(_x * mult, _y * mult);
    }

    string toString() const
    {
        return "(" + to_string(_x) + ", " + to_string(_y) + ")";
    }

    // -------- SURCHARGES INTERNES -----------------

    Point operator+ (Point const& other) const
    {
        return Point(_x + other._x, _y + other._y);
    }

    Point operator*(double mult) const
    {
        return Point(_x * mult, _y * mult);
    }

    Point& operator+=(Point const& other)
    {
        _x += other._x;
        _y += other._y;

        return *this; // on retourne la valeur point�e par l'objet courant
    }

    bool operator==(Point const& other) const
    {
        return (_x == other._x && _y == other._y);
    }

    bool operator!=(Point const& other) const
    {
        //return (_x != other._x || _y != other._y);

        return !(*this == other); // on d�finit le '!=' � partir du '=='
    }

    /* L'op�ratur d'affectation (p1 = p2) est le seul op�ratur universel : il est fourni par d�faut pourtoutes les classes
    * Il effectue par d�faut une copie de surface comme le constructeur de copie par d�faut.
    * Mais il est possible de le surcharger pour faire une copie profonde
    * Et il est �galement possible d'interdire son utilisation
    */

    Point& operator=(Point const& other) = delete; // On interdit l'utilisation de l'oparateur d'affectation

};

// -------- SURCHARGES EXTERNES -----------------


// Pourquoi rajouter "const" devant le type de retour ?
// Pour emp�cher de modifier le r�sultat de l'oparation
// - pour �viter de pouvoir faire (3*p) = ...
// - pour �viter de pouvoir faire ++(3*p)
const Point operator*(double mult, Point const& p)
{
    // Solution 1 : On passe par les getters
    // return Point(p.x() * mult, p.y() * mult);

    // Solution 2 : On acc�de directement aux attributs priv� en d�clarant la fonction en tant que 'friend' dans la classe
    // CECI EST UNE TRES MAUVAISE PRATIQUE : CA VIOLE LE PRINCIPE D'ENCAPSULATION
    // return Point(p._x * mult, p._y * mult);

    // Solution 3 : La multiplication �tant commutative, on appelle la surcharge interne avec l'op�rateur dans l'autre sens
    return p * mult;
}

ostream& operator<<(ostream& sortie, Point const& p)
{
    return sortie << p.toString();
}

int main()
{
    SetConsoleOutputCP(1252); 

    Point p1(2.0, 3.0);
    Point p2(12.0, -4.0);

    COUT(p1.toString());
    COUT(p2.toString());

    Point p3 = p1.add(p2).multiply(3.0); // p3 = (p1 + p2) * 3.0 => plus lisible !!

    COUT(p3.toString());

    p3 = (p1 + p2) * 3.0;

    COUT(p3.toString());

    Point p4 = 3.0 * (p1 + p2);

    COUT(p4.toString());

    p4 += p3;

    COUT(p4.toString());

    if (p1 == p1)
    {
        COUT("Les points sont identiques");
    }

    if (p1 != p2)
    {
        COUT("Les points sont diff�rents");
    }

    COUT(p2); // plus la peine d'appeler toString graec � la surcharge de l'op�rateur << 
    std::cout << p2 << std::endl;

    // p1 = p2; // ERREUR : on a interdit l'utilisation de l'op�rateur d'affectation
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

