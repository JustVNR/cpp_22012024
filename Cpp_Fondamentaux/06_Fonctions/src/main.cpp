#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15


/*
* Une fonction est un bloc d'instructions permettant d'�viter la duplication de code pour des traitements susceptibles d'�tre effectu�e � de multiples reprises.
* Elle rend le code plus facile � maintenir.
*
* Elle est d�finie par :
* - un type de retour
* - un nom
* - une liste de param�tres (�ventuellement vide)
* - un bloc d'instructions
*/

int Multiply(int a, int b)
{
	/* int res = a * b;

	 return res;*/

	return a * b;
}

// Fonction "Multiply" surcharg�e avec 3 param�tres

int Multiply(int a, int b, int c)
{
	return a * b * c;
}

int Sum(int a, int b = 4) // le param�tre "b" a une valeur par d�faut (4)
{
	return a + b;
}

double Sum(double a, double b)
{
	return a + b;
}

// D�claration de la fonction 'MultiplyAndLog' (impl�ment�e apr�s la fonction main)
// Une fonction qui ne retourne rien retourne "void"
void MultiplyAndLog(int a, int b); // Une fonction peut �tre impl�ment�e apr�s avoir �t� d�clar�e et appel�e
								   // MAIS ELLE DOIT �TRE DECLAREE AVANT D'ETRE APPELEE

double CelciusToFarenheit(double celcius)
{
	return celcius * 9 / 5 + 32;
}

// Fonction r�cursive
// Exemple la fonction factorielle : https://fr.wikipedia.org/wiki/Factorielle

unsigned long Factorielle(unsigned int n) {

	if (n == 1) return 1;

	// if (n == 2) return 1 * 2;
	/*if (n == 2) return Factorielle(n - 1) * n;
	if (n == 3) return Factorielle(n - 1 ) * n;*/

	return Factorielle(n - 1) * n;
}

// Par d�faut les param�tres de types primitifs pass� en param�tre des fonctions sont pass� 'par valeur'. C'est � dire que ce sont des COPIES des variables de la fonction appelante (dans le pr�sent la fonction 'main'). Ces copies n'exitent donc qu'� l'interieur du bloc d'instructions de la fonction.
void Increment(int n)
{
	n = n + 1; // n est une copie de la variable 'n' cr��e dans la fonction main. 
	// la variable n de main ne sera donc pas modifi�e.

	COUT("n APRES incr�mentation dans la fonction 'Increment' : " << n);
}

// Passage de param�tre par r�f�rence
void IncrementRef(int& n)
{
	n = n + 1;
}

void Swap(int& a, int& b)
{
	int temp = a;

	a = b;

	b = temp;
}

void MultiplyAndSum(int a, int b, int& multiplication, int& somme)
{
	multiplication = a * b;

	somme = a + b;
}
int Fibonacci(int n)
{
	// F0 = 0
	// if(n == 0) return 0;
	// 
	// F1 = 1
	// if(n == 1) return 1;
	
	if (n == 0 || n == 1)
	{
		return n;
	}
	//else facultatif
	//{
		// Fn = Fn-1 + Fn-2 pour n >= 2

		return Fibonacci(n - 1) + Fibonacci(n - 2);
	//}
}

int Fibonacci2(int n)
{
	int result;

	if (n == 0 || n == 1)
	{
		result = n;
	}
	else
	{
		result = Fibonacci(n - 1) + Fibonacci(n - 2);
	}

	return result;	
}

/*
* inline : le complitateur va directement remplacer le code de la fonction aux endroits ou elle est appel�e.
* Cela �vite de manipuler la m�moire comme c'est le cas lors de l'appel d'une fonction normale.
* Utiliser le mot cl� 'inline' est pertinent dans le cas de fonctions simples ( avec peu d'instructions) et appel�es souvent.
* 
* Dans le cadre de vrais projets, une fonction 'inline' est d�clar�e ET impl�ment�e dans un fichier .h.
* Elle pr�sente l'avantage par rapport aux macros "� la C" de pouvoir typer les variables.
*/
inline void SwapInline(int& a, int& b)
{
	int temp = a;

	a = b;

	b = temp;
}

int main()
{
	SetConsoleOutputCP(1252);

	int result = Multiply(2, 3);

	COUT("Multiply(2, 3) = " << Multiply(2, 3));

	result = Multiply(2, 3, 4);

	COUT("Multiply(2, 3, 4) = " << Multiply(2, 3));

	COUT("Sum(2, 3) = " << Sum(2, 3)); // 2 + 3 => 5

	COUT("Sum(2.3, 3.4) = " << Sum(2.3, 3.4));

	COUT("Sum(2) = " << Sum(2)); // 2 + 4 (par d�faut) => 6

	MultiplyAndLog(5, 6);

	// Exo : Fonction retournant une temp�rature en fahrenheit � partir d'une temp�rature en �celcius ( Formule F = C * 9/5 + 32)

	double farenheit = CelciusToFarenheit(23.5);

	COUT("CelciusToFarenheit(23.5) = " << farenheit);

	// EXO : Impl�menter une fonction qui incr�mente la valeur d'un entier

	int n = 0;

	COUT("n AVANT incr�mentation : " << n);

	Increment(n);

	COUT("n APRES incr�mentation : " << n); // 'n' n'a pas �t� incr�ment� � cause du passage de param�tre par valeur

	COUT("n AVANT incr�mentation par IncrementRef: " << n);

	IncrementRef(n);

	COUT("n APRES incr�mentation par IncrementRef : " << n); // 'n' a bien �t� incr�ment� grace au passage par r�f�rence

	// EXO : Impl�menter une fonction qui �change les valeurs de 2 nombres

	int a = 3;
	int b = 6;

	COUT("a = " << a << ", b = " << b);
	Swap(a, b);
	COUT("a = " << a << ", b = " << b);

	int mult, sum;

	MultiplyAndSum(a, b, mult, sum); // mult et sum sont pass�s par r�f�rence : on peut donc r�cup�rer leur valeur apr�s passage par la fonction 

	COUT("mult = " << mult << ", sum = " << sum);

	COUT("\n******** Fibonacci ************\n");
	// Afficher les 15 premiers termes de la suite de Fibonacci d�finie telle que : 
	// https://fr.wikipedia.org/wiki/Suite_de_Fibonacci


	for (int i = 0; i < 15; i++)
	{
		COUT("F(" << i << ") = " << Fibonacci(i));
	}

	// Fonction inline

	SwapInline(a, b);

	// return 0; la fonction main est un cas particulier qui retourne implcitement 0 par d�faut
}

// Impl�mentation de la fonction 'MultiplyAndLog'
void MultiplyAndLog(int a, int b)
{
	COUT(a << "  * " << b << " = " << (a * b));
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

