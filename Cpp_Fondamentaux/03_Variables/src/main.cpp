#include "stdafx.h"
#include <limits>

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

/*
* Une variable sert stocker et � acc�der � une donn�e en m�moire.
* 
* Une variable peut �tre de diff�rents types selon la taille de la donn�e qu'elle permet de stocker.
* 
* C++ prpose un certain nombres de types primitifs pemett   ant de d�finir des variables.
* 
* Chacun de ces types occupent une certaine taille (en octets) en m�moire.
* 
* 2 compilaateurs diff�rents pourront attribur des tailles diff�nretes � un m�me type.
* 
* Pour assurer la portabilit�, le stadart c++ d�finit n�anmoins une taille minimum pour chaque type.
* 
* - char 8 bites (1 byte) 1 octet
* - short 16 bits
* - int 16 bits
* - long 32 bits
* - long long 64 bits
* 
* R�gles de nommage des variables : 
* 
* - uniquement des lettrs et des chiffres et '_' (underscore)
* - ne peut commencer par un chiffre
* - pas de mot cl� r�serv� en tant que nom de variable
* - sensible � la casse 
* 
*/

int k = 45; // la variable k est une variable globale...

int main()
{
    SetConsoleOutputCP(1252); 

    bool myBoolNotInitialized; // D�claration d'une variable de type 'bool' nomm�e "myBoolNotInitialized"

    // IL FAUT TOUJOURS INITIALISER UNE VARIABLE AVANT DE L'UTILISER

    bool myBool = true; // D�claration et initialisation d'une variable de type 'bool' nomm�e "myBool"

    bool myBool2(false); // Syntaxe alternative

    COUT("sizeof(myBool) = " << sizeof(myBool)); // 1 octet

    char myChar = 'a';

    COUT("myChar = " << myChar);
    COUT("sizeof(myChar) = " << sizeof(myChar)); // 1

    myChar = 65; // Les caract�res sont cod�s sous formes d'enntiers correspondant � leur position dans la table ASCII

    COUT("myChar = " << myChar); // A

    TEXT_COLOR(GREEN);

    COUT("\n****************************************");
    COUT("********** TYPES ENTIERS **************");
    COUT("****************************************\n");

    int myInt = 12;

    myInt = 12 + 1; // Calcul 12 + 1 et affecte le r�sultat du calciul � la variable 'myInt'

    COUT("sizeof(int) : " << sizeof(int) << ", INT_MIN : " << INT_MIN << ", INT_MAX : " << INT_MAX);
    COUT("sizeof(unsigned int) : " << sizeof(unsigned int) << ", UINT_MAX : " << UINT_MAX);
    COUT("sizeof(short) : " << sizeof(short) << ", SHRT_MIN : " << SHRT_MIN << ", SHRT_MAX : " << SHRT_MAX);
    COUT("sizeof(long) : " << sizeof(long) << ", LONG_MIN : " << LONG_MIN << ", LONG_MAX : " << LONG_MAX);
    COUT("sizeof(long long) : " << sizeof(long long) << ", LLONG_MIN : " << LLONG_MIN << ", LLONG_MAX : " << LLONG_MAX);
    COUT("sizeof(unsigned long long) : " << sizeof(unsigned long long) << ", ULLONG_MAX : " << ULLONG_MAX);

    TEXT_COLOR(YELLOW);
    COUT("\n****************************************");
    COUT("********** TYPES FLOTTANTS **************");
    COUT("****************************************\n");

    // float myFloat = 1.1f;
    float myFloat = 1.1F;
    double myDouble = 1.1;

    myDouble = 1.1e3;

    COUT("myDouble = " << myDouble) << std::endl;

    /*
    * Il y a 3 types de nombes flottants : 
    * - float
    * - double
    * - long double
    */

    COUT("sizeof(float) : " << sizeof(float) << ", FLT_MIN : " << FLT_MIN << ", FLT_MAX : " << FLT_MAX);
    COUT("sizeof(double) : " << sizeof(double) << ", DBL_MIN : " << DBL_MIN << ", DBL_MAX : " << DBL_MAX);
    COUT("sizeof(long double) : " << sizeof(long double) << ", LDBL_MIN : " << LDBL_MIN << ", LDBL_MAX : " << LDBL_MAX);

    int a, b;

    std::cout << "Entrer une valeur enti�re pour 'a' :";
    std::cin >> a; // on r�cup�re la saisie de l'utilisateur gra�ce � cin (console in) et on le met dans la variable 'a'


    std::cout << "Entrer une valeur enti�re pour 'b' :";
    std::cin >> b; // idem pour 'b'

    COUT("a = " << a <<", b = " << b);

    // TODO : intervertir les 2 variables a et b

    int temp = a; // utilisation d'une variable temporaire pour stocker la valeur initiale de 'a'

    a = b;

    b = temp;

    COUT("a = " << a << ", b = " << b);

    TEXT_COLOR(GREEN);
    COUT("\n****************************************");
    COUT("********** CONVERSIONS DE TYPES **************");
    COUT("****************************************\n");

    int myInt2 = 2;

    long myLong = myInt; // Conversion implicite car le type 'long' est stock� sur davantage d'octets que le type 'int' (il y a donc la place de stocker l'un dans l'autre)

    myInt2 = (int)myLong; // Conversion explicite car 'int' est potentiellement plus petit que 'long' et il n'y a donc pas n�cessairement la place de stocker un long dans un int
    myInt2 = int(myLong); // syntaxe alternative


    TEXT_COLOR(YELLOW);
    COUT("\n****************************************");
    COUT("********** PORTEE DES VARIABLES **************");
    COUT("****************************************\n");

    // Une variable n'existe que dans le bloc d'instructions dans lequel elle est d�clar�e ( et dans tous les blocs d'instructions englobant)
    // Une instruction se termine par un ';'
    // Un bloc d'instructions est un ensemble d'instructions
    // Un bloc d'instructions est d�fini entr accolades : Exemple : {instrucion1; instruction2; etc...}
    // Une variable d�finie en dehors de tout bloc d'instructions est dite "globale"
    int i = 0;

    {
        COUT("i = " << i); // 0 : i est accessible car d�finie dans un bloc d'instructions englobant

        int j = 2;

        COUT("j = " << j); // 2

        // ATTENTION : Il est possible de d�finir une variable ayant le nom d'une variable d�finie dans un bloc d'instructions englobant

        int i = 3;

        COUT("i = " << i); // 3

        int k = 12; // une variable portant le m�me nom est d�j� d�finie en tant que variable globale (� l'ext�rieur de la fonction main)

        ::k = k + ::k; // on acc�de � la variable globale grace � l'op�rateur de r�solution de port�e (::) qui permet de d�masquer la variable globale
    }

    // COUT("j = " << j); // ERREUR 'j is undefined' 

    COUT("i = " << i); // 0
    COUT("k = " << k); // 57

    TEXT_COLOR(GREEN);
    COUT("\n****************************************");
    COUT("********** CONSTANTES **************");
    COUT("****************************************\n");

    const double lightSpeed = 300000; // Une constante doit �tre initialis�e au moment ou elle est d�clar�e
    // lightSpeed = 297000; ERREUR : une constante ne peut pas �tre modifi�e apr�s avoie �t� initialis�e

    COUT("lightSpeed = " << lightSpeed);

    TEXT_COLOR(WHITE);
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

