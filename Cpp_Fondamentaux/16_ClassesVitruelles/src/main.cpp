#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;

/*
* Il peut arriver qu'un classe soit incluse plusieurs fois dans une hi�rarchie de classes.
* 
* Exemple : Ovovivipare h�rite de Ovipare et de Vivipare, ces derni�res h�ritant elles-m�mes de Animal
* 
* Ainsi Ovovvivipare h�rite 2 fois de Animal.
* 
* Les ttributs et m�thodes de la classe Animal seront donc inclues 2 fois.
* Chaque objet de la classe Ovovivipare poss�dera 2 copies des attributs de la classe Animal.
* 
* Pour �viter cette duplication il faut d�clarer le lien d'h�ritage de la classe Animal avec ses clases filles Ovipare et Vivipare comme VIRTUEL.
* La classe Animal sera alors dite "virtuelle" ( � ne pas confondre avec une classe abstraite)
* 
* Syntaxe : class NomSousClass : public virtual NomSuperClasse
* 
* Cette m�thode pose n�anmoins un pb de conception. Car bien que ce soit la classe Ovovivipare qui pose probl�me, ce sont sont ses 2 classes m�res (Ovipare et Vivipare) qui devront h�riter virtuellement de la classe Animal.
* Ainsi les conceteurs des classes Ovipare et Vivipare doivent-ils tenir compte du fait qu'une classe pourra �ventuellement h�riter des 2.
* 
* Ce probl�me de conception explique que la quasi totalit� des langages objet interdisent l'h�ritage multiple. (Lui pr�f�rant le concept d'interface)
* 
* Rappel : dans un h�ritage usuel, le constructeur d'une sous-classe n'appelle que les constructeurs de ses super-classes imm�diates.
* 
* Dans un h�ritage virtuel, la super classe virtuelle (Animal) est initialis�e directement dans la sous... sous... sous... classe (Ovovivipare). Cette derni�re doit donc explicitement appeler le constructeur de la super... super... super classe. 
* 
*/

class Animal
{
protected:
    string _tete;

public:

    Animal(string const& description): _tete(description) { }
};

class Ovipare : public virtual Animal
{
protected:
    unsigned short _nOeufs;
public:

    Ovipare(unsigned short nOeufs) : Animal("A cornes"), _nOeufs(nOeufs) {}
};

class Vivipare : public virtual Animal
{
protected:
    unsigned short _gestation;

public:
    Vivipare(unsigned short gestation) : Animal("de poisson"), _gestation(gestation) {}
};

class Ovovivipare : public Ovipare, public Vivipare
{
protected:
    bool _especeProtegee;

public:
    
    Ovovivipare(unsigned short nOeufs, unsigned short gestation, bool protege = false);

    virtual ~Ovovivipare() {};

    void afficher() const
    {
        COUT("J'ai une t�te " << _tete);
    }
};

Ovovivipare::Ovovivipare(unsigned short nOeufs, unsigned short gestation, bool protege) :Animal("� cornes et de poisson"),  Vivipare(gestation), Ovipare(nOeufs), _especeProtegee(protege)
{
    /*
    * Les appels au constructeur de "Animal" par les constructeurs de "Ovipare" et "Vivipare" sont ignor�s.
    * Ils sont remplac�s par l'appel direct au constucteur de la super casse virtuelle Animal.
    * Cet appel direct, qui court-circuite l'appl au constructeur d'Anmail dans les constructeurs de Ovipare et Vivipare, doit �tre fait AVANT l'appel aux constructeurs des 2 classes directement h�rit�es (Ovipare et Vivipare)
    */
}



int main()
{
    SetConsoleOutputCP(1252); 

    Ovovivipare o(3, 4, false);

    o.afficher();
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

