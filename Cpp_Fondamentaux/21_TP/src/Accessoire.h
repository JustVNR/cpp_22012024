#pragma once
#include "Produit.h"
#include <memory>

class Accessoire : public Produit
{
	//private:
protected:
	const std::string _description;

public:
	Accessoire(std::string const& desc, double prix) : Produit(prix), _description(desc) {}

	virtual ~Accessoire() = 0; // Pour rendre la classe abstraite

	virtual void afficher(std::ostream& sortie) const override
	{
		sortie << _description << " coutant ";
		Produit::afficher(sortie); // Utilisation de :: pour d�masquer la m�thode "afficher" de la classe m�re
	}

	// copie polymorphique (� la fin seulement)
	virtual std::unique_ptr<Accessoire> copie() const = 0; // M�thode virtuelle pure
};

inline Accessoire::~Accessoire() {};	// Il faut d�finir le destructeur virtuel pur m�me si vide

class Bracelet : public Accessoire
{
public:

	Bracelet(std::string const& description, double prix) : Accessoire("bracelet " + description, prix) {}

	virtual ~Bracelet() { }

	/*Bracelet(Bracelet const& other) : Accessoire("bracelet " + other._description, other.prix())
	{
		std::cout << "TEST\n";
	}*/

	// copie polymorhique
	virtual std::unique_ptr<Accessoire> copie() const override
	{
		return (std::unique_ptr<Accessoire>) new Bracelet(*this); // *this => contenu point� par this <=> contenu de l'instance courante <=> appel au constructeur de copie implicite
	}
};

class Cadran : public Accessoire
{
public:

	Cadran(std::string const& description, double prix) : Accessoire("cadran " + description, prix) {}

	virtual ~Cadran() { }

	// copie polymorhique
	virtual std::unique_ptr<Accessoire> copie() const override
	{
		return (std::unique_ptr<Accessoire>) new Cadran(*this);
	}
};

class Fermoir : public Accessoire
{
public:

	Fermoir(std::string const& description, double prix) : Accessoire("fermoir " + description, prix) {}

	virtual ~Fermoir() { }

	// copie polymorhique
	virtual std::unique_ptr<Accessoire> copie() const override
	{
		return (std::unique_ptr<Accessoire>) new Fermoir(*this);
	}
};