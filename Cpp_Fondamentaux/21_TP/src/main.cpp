#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl

#define TEXT_COLOR(X) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),X)

#define COUT_COLOR(X, Y) {SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Y); \
std::cout << X << std:: endl; \
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);}

#define GREEN	10
#define CYAN	11
#define RED		12
#define MAGENTA	13
#define YELLOW	14
#define WHITE	15

using namespace std;

#include "montre.h"
/*
* - Les montres sont des produits
* - Les montres poss�dent :
*	- diff�rents accessoires (bracelet, cadran, fermoir...)
*	- un m�canisme de base
*
* - Les produits :
*	- ont un prix dont le calcul peut varier � partir d'une valeur de base
*	- sont affichables
*
* - Les m�canismes et accessoires sont aussi des produits.
*
* - Les accessoires ont une description
*
* - Il existe 3 sortes de m�canismes :
*	- analogiques
*	- digitaux
*	- hybrides (analogiques et digitaux)
*
* - On d�finira :
* 	- une classe "Mecanisme" de base poss�dant un attribut _heure (string)
*   - 2 classe filles de la classe "Mecanisme"
*		- "MecanismeAnalogique" qui poss�dera un attribut _date (string)
*		- "MecanismeDigital" qui poss�dera un attribut _reveil (string)
*   - 1 classe "MecanismeHybride" qui h�ritera des classes "MecanismeAnalogique" et "MecanismeDigital"
*/

int main()
{
	COUT("-------------------------------------------------------------");
	COUT("---------------- MONTRE SANS MECANISME ----------------------");
	COUT("-------------------------------------------------------------\n");

	// Produit p; // NOK : Produit est une classe abstraite (destructeur virtuel pur)

	Montre m; // Appel du constructeur sans param�tre

	m += new Bracelet("cuir", 20.0);
	m += new Fermoir("acier", 10.0);
	m += new Cadran("rond", 50.0);

	COUT(m);

	/*
	* Accessoires :
	* - bracelet cuir coutant 20
	* - fermoir acier coutant 10
	* - cadran rond coutant 50
	* Prix total = 80
	*/


	TEXT_COLOR(GREEN);
	COUT("\n----------------------------------------------------------");
	COUT("------------------ MECANISMES SEULS ----------------------");
	COUT("----------------------------------------------------------\n");

	// Test affichages

	MecanismeAnalogique analogique(215.00, 20220818);
	MecanismeDigital digital(98.00, "11:00", "7:00");
	MecanismeHybride hybride(412.50, "7:12", 20220818, "6:00");

	COUT(analogique);
	COUT("");
	COUT(digital);
	COUT("");
	COUT(hybride);

	/*
	* Mecanisme : analogique - Cadran : 12:00  - date : 20220818  - Prix : 215
	* Mecanisme : digital - Cadran : 11:00  - reveil : 7:00  - Prix : 98
	* Mecanisme : hybride
	*  - Cadran :
	*    - Analogique : 7:12  - date : 20220818
	*    - Digital : 7:12  - reveil : 6:00
	*  - Prix : 412.5
	*/

	TEXT_COLOR(YELLOW);
	COUT("\n-------------------------------------------------------");
	COUT("---------------- MONTRE COMPLETE ----------------------");
	COUT("-------------------------------------------------------\n");
	// Test montres

	Montre m2(new MecanismeHybride(412.50, "7:12", 20220818, "6:00"));

	m2 += new Bracelet("cuir", 20.0);
	m2 += new Fermoir("acier", 10.0);
	m2 += new Cadran("rond", 50.0);

	COUT(m2);


	/*
	* Accessoires :
	* - bracelet cuir coutant 20
	* - fermoir acier coutant 10
	* - cadran rond coutant 50
	*
	* Mecanisme : hybride
	*  - Cadran :
	*    - Analogique : 7:12  - date : 20220818
	*    - Digital : 7:12  - reveil : 6:00
	*  - Prix : 412.5
	* Prix total = 492.5
	*/

	TEXT_COLOR(GREEN);
	COUT("\n-----------------------------------------------------------");
	COUT("---------------- COPIE POLYMORPHIQUE ----------------------");
	COUT("-----------------------------------------------------------\n");

	COUT(" --------------------- Montre m3 = m2\n");

	Montre m3 = m2;

	COUT(m3);

	/*
	* Accessoires :
	* - bracelet cuir coutant 20
	* - fermoir acier coutant 10
	* - cadran rond coutant 50
	*
	* Mecanisme : hybride
	*  - Cadran :
	*        - Analogique : 7:12  - date : 20220818
	*        - Digital : 7:12  - reveil : 6:00
	*  - Prix : 412.5
	* Prix total = 492.5
	*/

	COUT(" --------------------- Montre m2 mise a l'heure :\n");

	m2.mettre_a_l_heure("7:13"); // seule m2 est mise � l'heure

	COUT(m2);

	/*
	* Accessoires :
	* - bracelet cuir coutant 20
	* - fermoir acier coutant 10
	* - cadran rond coutant 50
	*
	* Mecanisme : hybride
	*  - Cadran :
	*        - Analogique : 7:13  - date : 20220818
	*        - Digital : 7:13  - reveil : 6:00
	*  - Prix : 412.5
	* Prix total = 492.5
	*/

	COUT(" --------------------- Montre m3 non mise a l'heure :\n");

	COUT(m3);

	/*
	* Accessoires :
	* - bracelet cuir coutant 20
	* - fermoir acier coutant 10
	* - cadran rond coutant 50
	*
	* Mecanisme : hybride
	* - Cadran :
	*       - Analogique : 7:12  - date : 20220818
	*   	- Digital : 7:12  - reveil : 6:00
	* - Prix : 412.5
	* Prix total = 492.5
	*/
}
