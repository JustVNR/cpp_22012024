#pragma once
#include <iostream>

class Produit
{
private:

	double _prix;

public:

	Produit(double prix = 0.0) : _prix(prix) {}

	virtual double prix() const
	{
		return _prix;
	}

	virtual void afficher(std::ostream& sortie) const
	{
		sortie << prix(); // Peut avoir un comportement polymorphique grace � l'utilisation de prix() (et non pas _prix)
	}

	virtual ~Produit() = 0; // On d�finit le destructeur virtuel pur pour rendre la classe abstraite
};

inline Produit::~Produit() {};

inline std::ostream& operator<<(std::ostream& sortie, Produit const& p)
{
	p.afficher(sortie);

	return sortie;
}
