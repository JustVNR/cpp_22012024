#include "stdafx.h"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;

/*
* Un pointeur est une variable de type entier stockant une adresse m�moire
*
* Un pointeur peut servir � :
* - r�f�rencer un m�me objet depuis diff�rentes portions de conde sans avoir � le copier
* - manipuler un objet dont la dur�e de vie exc�de la port� du bloc d'instrucion courant (allocation dynamique)
* - pouvoir changer d'objet point�
*
* Il existe 3 types de pointeurs en C++ :
*
* - poiinteurs "� la C" : les plus puissants et (donc) le plus dangereux
* - les r�f�rences : tr�s s�res mais fondamentalement diff�rentes des vrais pointeurs
* - les pointeurs intelligents (smart pointers) depuis c++ 11. Requi�rent #include<memory>
*  Les pointeurs intelligents sont g�r�s par le/la d�veloppeur mais avec des gardes fous.
*
*   - unique_ptr : 2 unique_ptr diff�rents ne peuvent pointer simultan�ment sur le m�me objet
*   - shared_ptr : plusieurs shared_ptr puvent pointer la m�me adresse sans qu'aucun ne sache qu'elle n'est plus utiles aux autres
*   - weak_ptr : idem shared_ptr mais ne bloque pas la lib�ration de la m�moire
*/


void Increment(int val) {
	val++;
}

void IncrementReference(int& val) {
	val++;
}

/*
* Diff�rences entre un passage par pointeur et par r�f�rence :
* - un pointeur peut pointer sur null, pas ne r�f�rence
* - un pointeur peut �tre r�assign�, pas une r�f�rence
* 
* Si le param�tre est susceptible d'�tre null ou d'�tre r�assign� il faudra passer par pointeur
*/
void IncrementPointeur(int* val) {
	(*val)++;
}


struct Personne
{
	Personne()
	{
		COUT("New Person");
	}
	
	~Personne()
	{
		COUT("Delete Person");
	}
};

int main()
{
	SetConsoleOutputCP(1252);

	COUT("------------- Pointeurs � la C ---------------");

	int* ptr; // d�claraation 'un pointeur sur type entier
	ptr = NULL; // Un pointeur peut ne pointer sur rien
	ptr = nullptr; // depuis c++11

	int var = 8;

	ptr = &var; // Affectation de l'adresse de la varaible 'var' au pointeur 'ptr'

	*ptr = 10; // *ptr permet d'acc�der � la valeur point�e par 'ptr'. On parme de "d�r�f�rencement"

	COUT("var = " << var);

	int dereferenced = *ptr;

	COUT("dereferenced = " << dereferenced);

	int* ptr2 = nullptr;

	// Impossible de d�r�f�rencer un pointeur ne poitant sur rien sous peine de violation d'acc�s
	// *ptr2 = 10; // Exception thrown: write access violation.

	// Un tableau "� la C" d'eniters est un pointeur sur entier pointant le premier �l�m�ent du tableau
	int arr[] = { 1, 13, 25, 37, 45, 46 };

	int indice = 3;

	COUT("arr[" << indice << "] = " << *(arr + indice)); // indice 3 <=> 4i�me case => 37

	char bufferStack[8]{}; // Allocation automatique de 8 char dans la "Pile" non initialis�

	char* buffer = new char[8]; // Allocation dynamique (manuelle) dans le tas (heap). La variable 'buffer' contient l'adresse du d�but du tableau dans le tas

	COUT(*buffer); // � : non inialis�

	*buffer = 'a';
	COUT(*buffer); // a

	*(buffer + 1) = 'b';
	COUT(*(buffer + 1)); // b
	COUT(*(buffer + 2)); // � : non inialis�

	memset(buffer, 'z', 8); // Remplissage de la variable buffer avec un block de 8 * 1 Octet avec le carac�re 'z'

	for (size_t i = 0; i < 8; i++)
	{
		cout << *(buffer + i) << " "; // z z z z z z z z 
	}

	COUT("");
	char** ptrBuffer = &buffer; // "ptrBuffer" est un pointeur de pointeur stockant l'adresse du pointeur "buffer"


	// ATTENTION : A CHAQUE new / new[] doit correspondre un delete / delete[] pour lib�rer la m�moire dynamiquement allou�e dans le tas.
	delete[] buffer; // la zone m�moire est lib�r�e MAIS le poiteur pointe encore dessus
	buffer = nullptr; // Il est donc pr�f�rable de le faire pointer sur "null" apr�s "delete"


	COUT("------------- R�f�rences ---------------");

	/*
	* Une r�f�rence est un alias (du sucre syntaxique) masquant l'utilsation d'un pointeur pointant sur une variable EXISTANTE.
	* Une r�f�rence ne peut donc pas r�f�rencer null.
	* Une r�f�rence ne peut changer de variable r�f�renc�e
	*
	* ATTENTION :
	* Une r�f�rence n'est pas une variable : Mais une simple �tiquette sur une variable (existante)
	* Une r�f�rence ne peut donc elle-m�me pas �tre r�f�ren��e.
	*/

	int a;

	int& ref = a; // On peut d�sormais utiliser "ref" comme s'il s'agissait de la varaible qu'elle r�f�rence

	ref = 15;

	COUT("ref = " << ref); // 15
	COUT("a = " << a); // 15

	Increment(a); // passage de param�tre par valeur => copie du param�tre => a n'est pas incr�ment�
	COUT("a = " << a); // 15
	IncrementReference(a); // passage par r�f�rence => a est incr�ment�
	COUT("a = " << a); // 16
	IncrementPointeur(&a); // passage par pointeur => a est incr�ment�
	COUT("a = " << a); // 17

	const int& refReadOnly = a;

	// refReadOnly = 18; KO : impossible de modifier une variable via une r�f�rence sonstante

	a = 25; // toutes les r�f�renecs susceptibles de r�f�rencer 'a' changent de valeur
	COUT("ref = " << ref); // 25
	COUT("refReadOnly = " << refReadOnly); // 25

	int b = 10;

	ref = b; // ATTENTION !!!! ref ne r�f�rence pas 'b' puisqu'elle r�f�rence daj� 'a'. ref = b <=> a = b = 10

	COUT("a = " << a);

	// Si on veut pouvoir changer de variable r�f�renc�e il faut utiliser un vrai pointeur

	int* refPtr = &a;

	*refPtr = 12; // <=> a = 12

	refPtr = &b;
	*refPtr = 18; // <=> b = 18

	TEXT_COLOR(YELLOW);
	COUT("------------- SMART POINTERS ---------------");

	/*
	* Le pointeurs intelligents introduits en c++11 effectuent eux-m�me la d�sallocation de la m�moire au moment opportun via le "garbage collector" (ramasse miettes)
	*/

	COUT("\n------------- UNIQUE PTR ---------------\n");

	/*
	* "std::unique_ptr" poss�de la ressource vers laquelle il pointe => �vite les probl�me de propri�t� partag�e et simplifie la gestion des ressources
	*/

	{
		Personne* p = new Personne(); // Avec un pointeur classique "� la C"

		// utilisation de l'objet...

		delete p; // il faut lib�rer la m�moire manuellement

		p = nullptr;

		unique_ptr<Personne> uniqueP = make_unique<Personne>();

		// unique_ptr<Personne> uniqueP2 = uniqueP; // KO
	}

	TEXT_COLOR(CYAN);
	COUT("\n------------- SHARED PTR ---------------\n");

	{
		shared_ptr<Personne> sharedP;

		{
			shared_ptr<Personne> sharedP2 = make_shared<Personne>();

			sharedP = sharedP2; // OK puisqu'�tant de type shared_ptr
		} // sharedP2 ne sera pas lib�r� ici car sharedPtr en a encore besoin

		COUT("La m�moire allou�e par sharedP2 n'a pas encore �t� d�salou�e car encore utilse � sharedP");
	} // sharedP est lib�r� ici

	TEXT_COLOR(RED);
	COUT("\n------------- WEAK PTR ---------------\n");

	/*
	* utilis� pour �viter les pbs de r�f�rences circulaires
	* Lorsque plusieurs objets se r�f�rencent mutuellement via des shared_ptr, il peut y avoir des probl�mes de r�f�rence circulaire qui emp�che la lib�ration des ressources.
	*/
	{
		weak_ptr<Personne> weakP;

		{
			weak_ptr<Personne> weakP2 = make_shared<Personne>();
			weakP = weakP2; // OK puisqu'�tant de type weak_ptr
		} // weakP2 sera lib�r� ici car weakP ne bloque pas la lib�ration des resources

		COUT("La m�moire allou�e par weakP2 a d�j� �t� d�salou�e");
	} 
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

