#pragma once
class Point
{
private:
	double _x;
	double _y;

public:
	Point(double x, double y): _x(x), _y(y) { }

	double x() const
	{
		return _x;
	}

	void x(double value)
	{
		_x = value;
	}

	double y() const
	{
		return _y;
	}

	void y(double value)
	{
		_y = value;
	}
};