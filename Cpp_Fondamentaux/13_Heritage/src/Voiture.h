#pragma once
#include "Vehicule.h"

class Voiture : public Vehicule {
    int _nombrePortes;
public:
    Voiture(const std::string& marque, int nombrePortes) : Vehicule(marque), _nombrePortes(nombrePortes) {}

    void afficherDetails() const {
        Vehicule::afficherMarque();
        std::cout << "Type : Voiture" << std::endl;
        std::cout << "Nombre de portes : " << _nombrePortes << std::endl;
    }
};