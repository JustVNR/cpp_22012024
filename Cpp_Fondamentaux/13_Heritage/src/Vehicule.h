#pragma once
#include "stdafx.h"

class Vehicule {
    std::string _marque;

public:
    Vehicule(const std::string& marque) : _marque(marque) {}

    void afficherMarque() const {
        std::cout << "Marque : " << _marque << std::endl;
    }
};