#pragma once
#include "Point.h"
#include <string>
class Shape
{
protected:
	Point _position; // Aggr�gation : une "Shape" poss�de une position de type 'Point'

public:

	// Getter
	Point position()
	{
		return _position;
	}

	// Constructeur

	Shape(double x, double y): _position(x, y){ }

	// M�thodes

	void move(double x, double y);

	std::string toString();
};

// A mettre dans un Shape.cpp

void Shape::move(double x, double y)
{
	_position.x(_position.x() + x);
	_position.y(_position.y() + y);
}

std::string Shape::toString()
{
	return "(" + std::to_string(position().x()) + " , " + std::to_string(position().y()) + ")";
}

std::ostream& operator<<(std::ostream& sortie, Shape& s)
{
	return sortie << s.toString();
}