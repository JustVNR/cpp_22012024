#pragma once

#include "Vehicule.h"

class Moto : public Vehicule {
    std::string _type;

public:
    Moto(const std::string& marque, const std::string& type) : Vehicule(marque), _type(type) {}

    void afficherDetails() const {
        Vehicule::afficherMarque();
        std::cout << "Type : Moto" << std::endl;
        std::cout << "Type de moto : " << _type << std::endl;
    }
};