#pragma once
#include "Shape.h"
#include <string>

class Rectangle : public Shape
{
private:
	double _largeur;
	double _longueur;

public:

	Rectangle(double x, double y, double largeur, double longueur) : Shape(x, y), _largeur(largeur), _longueur(longueur)
	{ 
		// Commencer par appeler un constructeur de la classe m�re
		// Puis APRES SEULEMENT intialiser les attributs propre � la classe 'Rectangle'
	}
};