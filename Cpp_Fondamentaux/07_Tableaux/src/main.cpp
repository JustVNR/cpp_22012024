#include "stdafx.h"
#include <vector>
#include <array>

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

/*
* Un tableau est une collection de valeurs homog�nes ( de m�me type)
*
* Un tableau peut �tre :
* - statique : de taille fixe
* - dynamique : de taille variable
*/

using namespace std;


bool trouve(vector<int> vect, int value)
{
	for (auto& item : vect)
	{
		if (item == value) return true;
	}

	return false;
}

int main()
{
	SetConsoleOutputCP(1252);

	COUT("------------ TABLEAUX DYNAMIQUES ----------------");

	vector<int> myFirstVect; // D�claration d'un tableau / vecteur dynmaique d'entiers vide.

	if (myFirstVect.empty()) {

		myFirstVect.push_back(4); // ajoute un �l�ment en fin de tableau
		myFirstVect.push_back(5);

		COUT("myFirstVect.size() = " << myFirstVect.size()); // retourne la taille du tableau
		COUT("myFirstVect.front() = " << myFirstVect.front()); // retourne une r�f�rence vers le premier �l�ment du tableau
		COUT("myFirstVect.back() = " << myFirstVect.back()); // retourne une r�f�rence vers le dernier �l�ment du tableau

		myFirstVect.pop_back(); // supprime le dernier �l�ment du tableau

		myFirstVect.clear(); // vide le tableau

		COUT("myFirstVect.size() = " << myFirstVect.size()); // 0
	}

	vector<int> vect(5); // Initialisation d'un vector de 5 entiers (par d�faut � 0)

	COUT("vect[4] = " << vect[4]); // 0

	vector<int> vectOnes(5, 1); // Initialisation d'un vector de 5 entiers initialis�s � 1

	COUT("vectOnes[4] = " << vectOnes[4]); // 1

	vector<int> vectCopy(vectOnes); // Cr�ation d'un vecteur par copie d'un vecteur existant de m�me type.
	// vector<int> vectCopy = vectOnes; syntaxe alternative

	vector<int> vectAges = { 20, 35, 40, 75, 80 };

	vectOnes = vectAges;

	vectAges[0] = 30;


	COUT("\nPARCOURS DE VECTOR AVEC BOUCLE FOR\n");

	for (int i = 0; i < vectOnes.size(); i++)
	{
		COUT("vectOnes[" << i << "] = " << vectOnes[i] << " vectAges[" << i << "] = " << vectAges[i]);
	}

	COUT("\nPARCOURS DE VECTOR AVEC BOUCLE FOR EACH\n");

	for (auto item : vectOnes)
	{
		item++; // ATTENTION : le tableau n'est pas modifi� car la variable "item" est une copie de la valeur du tableau
		cout << item << " ";
	}

	COUT("\n---------------\n");

	for (auto item : vectOnes)
	{
		cout << item << " ";
	}

	COUT("\n------ PASSAGE PAR REFERENCE ---------\n");
	// Parcours du tableau avec passage par r�f�rence
	for (auto& item : vectOnes)
	{
		item++; // le tableau sera bel et bien modifi�
		cout << item << " ";
	}

	COUT("\n---------------\n");

	for (auto item : vectOnes)
	{
		cout << item << " ";
	}


	myFirstVect.swap(vectAges); // Echange les 2 tableaux ( "myFirstVect" �tant vide, vectAges est d�sormais vide)

	COUT("\n------myFirstVect aprs swap avec vectAges--------\n");

	for (auto item : myFirstVect)
	{
		cout << item << " ";
	}

	COUT("\nvectAges.size() = " << vectAges.size()); // 0

	COUT("\n--------------- TABLEAUX DYNAMIQUES MULTIDIMENSIONELS (TABLEAUX DE TABLEAUX)-----------------\n");

	vector<vector<int>> vect2D(5, vector<int>(6, 1)); // Tableau de 5 tableaux de 6 entiers initialis�s avec la valeur 1

	vect2D[2][3] = 0; // 3i�me ligne - 4 i�me colonne (ou 4i�me �l�ment du 3i�me tableau)

	for (auto& line : vect2D)
	{
		for (auto& col : line)
		{
			cout << col << " ";
		}
		COUT("");
	}
	COUT("----------------");
	// Un tableau de tableaux peut contenir des tableaux de taille diff�rentes
	vect2D[1].push_back(5); // ajoute la valeur 5 en fin de second tableau (ligne)

	for (auto& line : vect2D)
	{
		for (auto& col : line)
		{
			cout << col << " ";
		}
		COUT("");
	}
	COUT("----------------");
	vector<vector<int>> vect2D2 = { { 1, 2, 3}, { 4,5,6,7} , {8, 9} };

	for (auto& line : vect2D2)
	{
		for (auto& col : line)
		{
			cout << col << " ";
		}
		COUT("");
	}

	/*
	* Demander � l'utilisateur des nombres entiers tant qu'il souhaite en saisir
	* Calculer la somme et la moyenne des nombres saisis
	*
	* // Entrez un nombre entier : 4
	* // Voulez vous ajouter un autre nombre ? ( O / N)
	*
	*/

	if (false)
	{
		int nombre;

		vector<int> nombres;

		char continuer;

		do
		{
			cout << "Entrez un nombre entier : ";

			cin >> nombre;

			nombres.push_back(nombre);

			cout << "Voulez vous ajouter un autre nombre ? ( O / N) ";

			cin >> continuer;

			// } while (continuer == 'O' || continuer == 'o');
		} while (toupper(continuer) == 'O');

		if (nombres.empty())
		{
			COUT("Aucun nombre saisi");
		}
		else
		{
			int somme = 0;

			for (int num : nombres)
			{
				somme += num;
			}

			double moyenne = (double)somme / nombres.size();

			COUT("Somme des nombres :" << somme);
			COUT("Moyenne des nombres :" << moyenne);
		}
	}

	COUT("\n------------------- TABLEAUX STATIQUES -----------------------\n");

	/*
	* Un tableau statique a une taille fixe.
	*
	* Il existe 2 types de tableaux fixes :
	* - tableau "� la C"
	* - array (introduit c++ 11)
	*
	* Les tableaux � la C :
	* - sont toujours pass�s par r�f�rence
	* - n'ont pas connaissance de leur propre taille
	* - ne peuvent pas �tre manipul�s globalement ( pas de tab1= tab2)
	* - ne peuvent pas �tre retourn� par une fonction
	*/

	COUT("--------- TABLEAUX � la C ---------------\n");

	const int size = 3;

	int cStyleArray[size]; // d�claration d'un tableau de "size" (const) entiers non initialis�.

	int cStyleArray2[size] = { 1, 2, 3 }; // // d�claration d'un tableau de "size" (const) entiers initialis�

	int calculatedSize = sizeof(cStyleArray) / sizeof(cStyleArray[0]); // D�termination de la taille du tableau � post�riori

	for (int i = 0; i < calculatedSize; i++)
	{
		cStyleArray[i] = i;
	}

	for (int i = 0; i < calculatedSize; i++)
	{
		cout << cStyleArray[i] << " ";
	}

	COUT("");

	COUT("\n---------- TABLEAUX � la \"C\" 2D--------------\n");

	const int nrows = 3;
	const int ncols = 4;

	int cStylArray2D[nrows][ncols];

	/*for (int i = 0; i < nrows; i++)
	{
		for (int j = 0; j < ncols; j++)
		{
			cStylArray2D[i][j] = i + j;
		}
	}*/

	for (int i = 0; i < nrows; i++)
		for (int j = 0; j < ncols; j++)
			cStylArray2D[i][j] = i + j;


	for (int i = 0; i < nrows; i++)
	{
		for (int j = 0; j < ncols; j++)
		{
			cout << cStylArray2D[i][j];
		}
		COUT("");
	}
	TEXT_COLOR(GREEN);
	COUT("\n---------- ARRAY --------------\n");

	array<double, 4> array4; // D�claration d'un array (non initialis�) de 4 doubles

	// un array non initialis� contient des valeurs inconnues
	for (auto& item : array4)
	{
		cout << item << " ";
	}
	COUT("");

	array<unsigned int, 3> array3 = { 1, 2, 3 };
	// array<unsigned int, 3> array3({ 1, 2, 3 }); // syntaxe alternative

	array<unsigned int, 3> array3b = array3; // recopie les valeurs de array3 dnas array3b

	array3b[array3b.size() - 1] = 10; // Changment de valeur du dernier �l�ment du tableau array3b.

	for (int i = 0; i < array3b.size(); i++)
	{
		cout << array3b[i] << " ";
	}


	COUT("\n---------- ARRAY de ARRAYS --------------\n");

	array<array<int, 2>, 4> array2D; // array de 4 arrays de 2 entiers
	array<array<array<int, 5>, 2>, 4> array3D; // array de 4 arrays de 2 arrays de 5 entiers

	array3D[0][0][0] = 1;

	for (int i = 0; i < array3D.size(); i++)
	{
		for (int j = 0; j < array3D[i].size(); j++)
		{
			for (int k = 0; k < array3D[i][j].size(); k++)
			{
				array3D[i][j][k] = i + j + k;
			}
		}
	}

	// array2D = { {1, 2} , { 3, 4} , {5, 6} , {7,8} }; // ATTENTION : on ne peut pas imbriquer les accolades
	array2D = { 1, 2, 3, 4 , 5, 6 , 7, 8 };

	for (auto& line : array2D)
	{
		for (auto& col : line)
		{
			cout << col << " ";
		}
		COUT("");
	}


	COUT("\n---------- EXO MAXIMUM ----------------\n");
	/*
	* Utiliser un array pour stocker un ensemble de nombres entiers saisis par l'utilisateur.
	* Trouver et afficher le maximum saisi
	*/

	if (false)
	{
		const int taille = 5;
		array<int, taille> nombres2;

		COUT("Entrez " << taille << " nombres entiers : ");

		for (int i = 0; i < taille; i++)
		{
			cout << "Nombre " << (i + 1) << " : ";
			cin >> nombres2[i];
		}

		int maximum = nombres2[0];

		for (int i = 1; i < taille; i++)
		{
			if (nombres2[i] > maximum) {
				maximum = nombres2[i];
			}
		}

		COUT("Le maximum parmi les nombres saisis est : " << maximum);
	}


	COUT("\n---------- EXO ELEMENTS UNIQUES ----------------\n");
	/*
	* Utiliser un array pour stocker un ensemble de nombres entiers.
	* Trouver et afficher tous les �l�ments uniques dans le tableau. (Supprimer les doublons)
	*/

	array<int, 9> duplicates = { 1,2,2,3,4,5,5,5,6 };

	vector<int> uniques;


	for (int item : duplicates)
	{
		if (!trouve(uniques, item)) // si je ne trouve PAS item dans uniques
		{
			uniques.push_back(item); // je le rajoute dans uniques
		}
	}

	for (auto item : uniques)
	{
		cout << item << " ";
	}


	TEXT_COLOR(YELLOW);

	COUT("\n---------------- STRING -------------------\n");

	string maChaine; // d�claration d'une chaine vide

	string message = "Hello World";
	// string message("Hello World"); // syntaxe alternative

	char myChar = 'c';
	//myChar = "d"; // KO

	maChaine = 'a'; // OK

	maChaine = "Hello";

	maChaine += " World"; // Concat�nation

	COUT(maChaine);

	maChaine[0] = 'h'; // Une chaine est un tableau de caract�res et on peut donc acc�der � chacun des caract�res comme aux �l�ments d'un tableau

	COUT(maChaine);

	maChaine.append(" !!");

	COUT(maChaine);

	COUT("maChaine.size() = " << maChaine.size());

	maChaine.insert(5, " F___ING");

	COUT(maChaine);

	maChaine.replace(5, 8, " WONDERFULL"); // 5 indice de d�part - 8 : longueur de la sous chaine remplac�e

	COUT(maChaine);

	if (maChaine.find("o") != string::npos)
	{
		COUT("maChaine.find(\"o\") = " << maChaine.find("o")); // retourne l'indice de la premi�re occurence
		COUT("maChaine.rfind(\"o\") = " << maChaine.rfind("o")); // retourne l'indice de la derni�re occurence
	}


	COUT("maChaine.substr(6, 10) = " << maChaine.substr(6, 10)); // retourne la sous chaine de longeur 10 � partir de l'indice 6 
}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

