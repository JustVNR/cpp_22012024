#include "stdafx.h"
#include<array>
#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

using namespace std;
// |_|_|_|_|_|_|_|
//
// |_|_|_|_|_|_|_|
//
// |_|_|_|_|_|_|_|
//
// |_|_|O|_|_|_|_|
//
// |_|_|X|_|_|_|_|
//
// |_|_|X|O|_|_|_|
//
// =1=2=3=4=5=6=7=

enum Pion{ROUGE, JAUNE, VIDE};

typedef array<array<Pion, 7>, 6> Grille;

void Initialise(Grille& g)
{
	for (auto& line : g)
	{
		for (auto& c : line)
		{
			c = Pion::VIDE;
		}
	}
}

void Affiche(const Grille& grille) {

	for (auto& ligne : grille)
	{
		cout << endl << " |";

		for (auto& colonne : ligne)
		{
			switch (colonne)
			{
			case Pion::JAUNE:
				cout << 'X';
				break;
			case Pion::ROUGE:
				cout << 'O';
				break;
			case Pion::VIDE:
				cout << '_';
				break;
			default:
				cout << 'E';
				break;
			}
			cout << '|';
		}
		cout << endl;
	}

	cout << endl << ' ';
	for (size_t i = 0; i < grille[0].size(); i++)
	{
		cout << '=' << i + 1;
	}
	cout << "=" << endl << endl;
}

bool Plein(const Grille& grille) {

	/*for (size_t i = 0; i < grille[0].size(); i++)
	{
		if (grille[0][i] == Pion::VIDE) return false;
	}*/

	for (auto c : grille[0]) // Il suffit de tester la ligne du haut
	{
		if (c == Pion::VIDE) return false; // si au moins une case est Pion::VIDE => la grille n'est pas pleine => return false
	}
	return true;
}
bool Jouer(Grille& g, size_t col, Pion p)
{
	if (col >= g[0].size() || g[0][col] != Pion::VIDE) return false; // Le num�ro de colonne est invalide ou la colonne est pleine

	// Il faut trouver � quelle hauteur (ligne) le pion va tomber.
	// Il faut donc trouver la prem�re case vide en partant du bas dans la colonne consid�r�e
	size_t line = g.size() - 1; // on commence par la ligne du bas et on remonte tant que la case n'est pas vide

	while ((g[line][col] != Pion::VIDE))
	{
		line--; // on remonte tant que la case n'est pas vide
	}

	g[line][col] = p; // on met le pion dans la case

	return true; // le coup est valide
}
void DemandeEtJoue(Grille& grille, Pion joueur)
{
	/*if (joueur == Pion::JAUNE)
	{
		cout << "Joueur X : entrez un num�ro de colonne." << endl;
	}
	else
	{
		cout << "Joueur O : entrez un num�ro de colonne." << endl;
	}*/

	COUT("Joueur " << (joueur == Pion::JAUNE ? "X" : "O") << " : entrez un num�ro de colonne.");

	size_t col;
	cin >> col;
	col--; // les indices des colonnes commencent � 0

	while (not Jouer(grille, col, joueur)) // Si la fonction Jouer retourne false c'est que le coup est invalide
	{
		COUT("Coup invalide.");

		COUT("Joueur " << (joueur == Pion::JAUNE ? "X" : "O") << " : entrez un num�ro de colonne.");

		cin >> col;
		col--;
	}
}



size_t Compte(const Grille& g, size_t x, size_t y, int dirX, int dirY) // dirX et dirY peuvent valoir -1,0 ou 1
{
	size_t count = 0;

	//Coordonn�es du point � comparer
	size_t newX = x;
	size_t newY = y;

	// tant que le pion de la case(x,x) est �gal au pion de la case(newX, newY) ET que la case (newX, newY) est dans la grille
	while (newX < g.size() && newY < g[0].size() && g[newX][newY] == g[x][y])
	{
		count++; // On ingr�mente le compteur 
		newX += dirX; // Et on d�calle le point adjacent � comparer...
		newY += dirY; // dans la direction d�finie par dirX et dirY
	}

	return count;
}

bool Gagne(Grille& g, Pion joueur)
{
	for (size_t line = 0; line < g.size(); line++)
	{
		for (size_t col = 0; col < g[line].size(); col++)
		{
			if (joueur == g[line][col])
			{
				if (Compte(g, line, col, -1, +1) >= 4) return true;
				if (Compte(g, line, col, 0, +1) >= 4) return true;
				if (Compte(g, line, col, +1, +1) >= 4) return true;
				if (Compte(g, line, col, +1, 0) >= 4) return true;
			}
		}
	}
	return false;
}

int main()
{
    SetConsoleOutputCP(1252); 

    Grille grille;

    Initialise(grille);

	Affiche(grille);

	Pion joueur = Pion::JAUNE;

	bool gagne = false;

	do
	{
		DemandeEtJoue(grille, joueur);

		Affiche(grille);

		gagne = Gagne(grille, joueur);

		joueur = (joueur == Pion::JAUNE) ? Pion::ROUGE : Pion::JAUNE;

	} while (not gagne and not(Plein(grille)));

	if (gagne == true)
	{
		COUT("joueur " << (joueur == Pion::JAUNE ? "O" : "X") << " a gagn�");
	}
	else
	{
		COUT("Match null");
	}

}

/* **************************************************
 * ****************** RACCOURCIS ********************
 * **************************************************
 *
 * - CTRL + Q : rechercher
 * - CTRL + C : copie la ligne enti�re
 * - CTRL + X : coupe la ligne enti�re
 * - CTRL + ENTER : ajoute une ligne au dessus
 * - CTRL + SHIFT + ENTER : ajoute une ligne en dessous
 * - CTRL + K CTRL + C : Commenter
 * - CTRL + K CTRL + U : D�commenter
 * - CTRL + R CTRL + R : Renommer
 * - F12 : aller � la d�finition
 * - SHIFT + F12 : trouver toutes les r�f�rences
 * - CTRL + R CTRL + M : cr�er une m�thode � partir d'une s�lection
 * - ALT + UP/DOWN : d�placer une ligne / s�lection
 * - ALT + CAPSLOCK + UP/DOWN : d�placer une ligne / s�lection ???
 * - CTRL + K CTRL + D : formater
 * - CTRL + K CTRL + S : surround s�lection avec structure de controle (if, while...)
 * - CTRL + M CTRL + O : collapse all
 * - CTRL + R CTRL + G : r�organiser les using
 * - CTRL + . : ajouter une/des m�thode(s)
 * - CTRL + , : rechercher un fichier
 * - CTRL + T : rechercher un fichier
 * - CTRL + LEFT/RICHT : se d�placer sur une ligne mot par mot
 * - CTRL + SHIFT + LEFT/RICHT : se d�placer et s�lectionner une ligne mot par mot
 * - ALT + SHIFT + . : s�lectionner un mot ( + . s�lectionner le mot suivant etc...)
 * - ALT + SHIFT + UP/DOWN . : �tendre / restreindre le curseur ( ESC pour sortir)
 * - CTRL + K CTRK + B : ouvrir le gestionnaire de snippets
 * - CTRL + (SHIFT +) TAB : naviguer parmi les onglets
 * Running and debugging :
 *
 * - F5 : d�bogguer l'application
 * - CTRL + F5 : lancer l'application
 * - SHIFT + F5 : stopper l'application
 * - F9 : ajouter / supprimer un breakpoint
 * - F10 : step over
 * - F11 : step into
 * - SHIFT + F11 : step out
 *
 * Sp�cial C/C ++ :
 *
 * - CTRL + K CTRL + O : afficher ou masquer le header file
 */

