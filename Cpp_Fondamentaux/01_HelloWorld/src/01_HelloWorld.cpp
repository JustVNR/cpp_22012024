// 01_HelloWorld.cpp : This file contains the 'main' function. Program execution begins and ends there.


// includes d�plac�s dans stdafx.h
//#include <iostream> // input outut stream : contient des utilitaires pour la gestion et la manipulation de flux d'entr�e / sortie
//#include "windows.h"

#include "stdafx.h"

/*
* #define : MACRO : directive du preprocesseur permettant d'inclure le contenu du fichier pass� en param�tre avant l'appel du compilateur
*/

#define COUT_HELLO_WORLD std::cout << "Hello World!\n"

#define COUT(X) std::cout << X << std::endl
#define TEXT_COLOR(C) SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), C)

#define GREEN 10
#define CYAN 11
#define RED 12
#define MAGENTA 13
#define YELLOW 14
#define WHITE 15

/*
* Il existe plusieurs conventions de nommage utilis�es pour les langages de d�veloppement
* 
* camelCase : maVariable
* PascaleCase : MaVariable
* snake_case : ma_variable
*/
int main()
{
    // cout : Console out : flux de sortie de la console
    // std : namespace "std"
    // // :: : op�rateur de r�solution de port�e
    // << : left shift operator : op�rateur de rediction de flux

    std::cout << "Hello World!\n";

    COUT_HELLO_WORLD;

    COUT("Hello world from macro with on parameter");


    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10); // CODE SPECIFIC A WINDOWS => #include "windows.h" 

    COUT("Hello world in GREEN");

    TEXT_COLOR(12);

    COUT("Hello world in another color");

    TEXT_COLOR(YELLOW);

    COUT("Hello world in yellow");

    COUT("J'�cris en fran�ais avec des caradct�res accentu�s");

    SetConsoleOutputCP(1252); // Pour tenir compte des caract�res fran�ais

    COUT("J'�cris en fran�ais avec des caradct�res accentu�s");

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
